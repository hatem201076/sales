(function ($) {
  $(document).ready(() => {
    function readURL(input, target) {
      // if input contains at least one file.
      if (input.files && input.files[0]) {
        // instaltiate a new file reader instance.
        const reader = new FileReader();
        // on reader load.
        reader.onload = function (e) {
          // set target image's src to the selected image.
          target.attr('src', e.target.result);
        };
        // read the selected image as data url.
        reader.readAsDataURL(input.files[0]);
      }
    }

    $(document).on('change', 'input.file-styled', function () {
      const target = $(`#${$(this).attr('data-target')}`);
      if (target) {
        readURL(this, target);
        // Display the filepath to user
        $(this).parent().find('.filename').text($(this).val());
      }
    });

        // set width and/or height to its corresponding props.
    $('img.file-styled-target').each(function (index) {
      if ($(this).attr('data-height')) {
        $(this).css('height', $(this).attr('data-height'));
      }

      if ($(this).attr('data-width')) {
        $(this).css('width', $(this).attr('data-width'));
      }
    });
  });
}(jQuery));
