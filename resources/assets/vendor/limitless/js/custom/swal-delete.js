function post(path, params, method) {
  method = method || 'post'; // Set method to post by default if not specified.

  // The rest of this code assumes you are not using a library.
  // It can be made less wordy if you use one.
  const form = document.createElement('form');
  form.setAttribute('method', method);
  form.setAttribute('action', path);

  for (const key in params) {
    if (params.hasOwnProperty(key)) {
      const hiddenField = document.createElement('input');
      hiddenField.setAttribute('type', 'hidden');
      hiddenField.setAttribute('name', key);
      hiddenField.setAttribute('value', params[key]);

      form.appendChild(hiddenField);
    }
  }

  document.body.appendChild(form);
  form.submit();
}


const locale = (function locale() {
  const documentLang = document.documentElement.lang;

  if (documentLang === 'ar') {
    return {
      confirmButtonText: 'نعم! حذف',
      cancelButtonText: 'الغاء',
    };
  }

  return {
    confirmButtonText: 'Yes, Delete It!',
    cancelButtonText: 'Cancel',
  };
}());

$(document).on('click', '.delete-confirm', function (e) {
  const url = $(this).attr('data-url');
  const title = $(this).attr('data-title') || 'Are you sure do you want to delete this resource?';
  const message = $(this).attr('data-message') || 'You cannot undo this action!';
  const type = 'warning';

  swal({
    title,
    text: message,
    type,
    showCancelButton: true,
    confirmButtonColor: '#DD6B55',
    confirmButtonText: locale.confirmButtonText,
    cancelButtonText: locale.cancelButtonText,
    closeOnConfirm: false,
  }, () => {
    post(url, {
      _token: document.head.querySelector('meta[name="csrf-token"]').content,
      _method: 'DELETE',
    }, 'post');
  });

  // prevent defult action from being executed.
  e.preventDefault();
});
