(function ($) {
  $(document).ready(() => {
    /*
    Components Initialization
     */
    $('.form-group.has-error:first input:first').focus();

    // Enable jQuery Are-You-Sure Plugin for dirty forms checking
    // Enable on all forms
    $('form').areYouSure();

    // Default initialization
    $('.styled, .multiselect-container input').uniform({
      radioClass: 'choice',
    });

    // Ckeditor initialization
    $('.html-editor').each((i, el) => {
      CKEDITOR.replace(el.id, {
        language: document.documentElement.lang,
      });
    });

    // Bootstrap Tags input initialization
    $('[data-role="tagsinput"], .tags-input').tagsinput();
  });
}(jQuery));
