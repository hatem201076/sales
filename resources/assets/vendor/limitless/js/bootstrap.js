/* eslint-disable no-multi-assign */
/*
 Load limitless core javascript files
 */

// eslint-disable-next-line no-global-assign
window.jQuery = $ = require('./core/libraries/jquery.min');
require('./core/libraries/bootstrap.min');
require('./core/app');


/*
  Load Limitless Plugins
 */

require('./plugins/loaders/pace.min');
require('./plugins/loaders/blockui.min');
require('./plugins/notifications/sweet_alert.min');
require('./plugins/forms/styling/uniform.min');
require('./plugins/forms/selects/bootstrap_select.min');
require('./plugins/forms/tags/tagsinput.min');
require('./plugins/pickers/anytime.min');
window.Dropzone = require('./plugins/uploaders/dropzone.min');

/*
 Load Custom Scripts
 */

require('./custom/libs/jquery.are-you-sure');
require('./custom/swal-delete');
require('./custom/uploader-image');
require('./custom/helpers');
