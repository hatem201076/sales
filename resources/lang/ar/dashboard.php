<?php

return [
    'navbar' => [
        'links' => [
            'website' => 'الموقع',
        ],
    ],

    'sidebar' => [
        'links' => [
            'home' => 'الرئيسية',
        ],
    ],

    'components' => [
        'forms' => [
            'tagsinput' => [
                'help' => 'افصل بين الكلمات بعلامة :separator',
            ],
            'imageinput' => [
                'no-file-selected' => 'لم يتم اختيار صورة',
                'choose' => 'اختيار',
                'help' => 'قم بالضغط علي "اختيار" ثم اختار صورة...',
            ],
        ],
    ],
];
