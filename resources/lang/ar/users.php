<?php

use App\Models\User;

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'male' => 'ذكر',
    'female' => 'أنثى',

    'singular' => 'المستخدم',
    'plural' => 'المستخدمين',
    'info' => 'بيانات المستخدم',


    'actions' => [
        'add' => 'إضافة مستخدم',
        'edit' => 'تعديل مستخدم',
        'delete' => 'حذف مستخدم',
    ],
    'notification' => [
        'verified' => 'تم التفعيل',
    ],


    'messages' => [
        'created' => 'تم إنشاء مستخدم بنجاح.',
        'updated' => 'تم تعديل بيانات مستخدم بنجاح.',
        'deleted' => 'تم حذف مستخدم بنجاح.',
        'ask' => [
            'delete-title' => 'هل أنت متأكد من أنك تريد حذف :user؟',
            'delete-info' => 'لا يمكنك التراجع عن هذه الخطوة!',
        ],
    ],

    'type' => [
        User::MANAGER => 'مدير',
        User::USER => 'مستخدم',
        User::POSS => 'مدير',
    ],

    'dialogs' => [
        'delete' => [
            'title' => 'هل أنت متأكد من أنك تريد حذف المستخدم (:item)؟',
            'info' => 'لا يمكنك التراجع عن هذه الخطوة!',
        ],
    ],
    'attributes' => [
        'name' => 'الإسم',
        'email' => 'البريد الالكتروني',
        'phone' => 'الهاتف',
        'password' => 'كلمة المرور',
        'type' => 'نوع المستخدم',
        'password_confirmation' => 'تأكيد كلمة المرور',
        'is_admin' => 'مدير؟',
        'avatar' => 'صورة الملف الشخصي',
        'roles' => 'الصلاحيات',
    ],
];
