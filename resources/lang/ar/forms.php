<?php

return [
    'add' => 'اضافة',
    'save' => 'حفظ',
    'edit' => 'تعديل',
    'delete' => 'حذف',
    'show' => 'عرض',
    'active' => 'تفعيل',
    'sale' => 'بيع',
    'installment' => 'تقسيط',
    'count' =>'إحـسب'
];
