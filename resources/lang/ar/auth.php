<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'بيانات الاعتماد هذه غير متطابقة مع البيانات المسجلة لدينا.',
    'throttle' => 'عدد كبير جدا من محاولات الدخول. يرجى المحاولة مرة أخرى بعد :seconds ثانية.',
    'not_active' => 'الحساب غير مفعل',

    'sentences' => [
        'Login to your account' => 'قم بتسجيل الدخول لحسابك',
        'Enter your credentials below' => 'ادخل بيانات حسابك',
        'Remember me' => 'تذكرني',
        'Forgot password' => 'نسيت كلمة المرور؟',
        'Send Password Reset Link' => 'ارسل رابط استعادة كلمة المرور',
        'Reset Password' => 'استعادة كلمة المرور',
    ],

    'actions' => [
        'login' => 'تسجيل دخول',
        'logout' => 'تسجيل خروج',
        'register' => 'تسجيل',
    ],

];
