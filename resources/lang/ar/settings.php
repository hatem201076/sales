<?php

return [

    'singular' => 'الاعدادات',
    'plural' => 'الاعدادات',
    'title' => ' الموقع',

    'actions' => [
        'view' => 'مشاهدة اعدادات الموقع',
    ],
    'messages' => [
        'saved' => 'تم حفظ الاعدادات الجديده',
    ],
    'attributes' => [
        'title' => ' عنوان الموقع',
        'phone' => 'الهاتف',
        'email' => 'البريد الالكتروني',
        'youropinion' => 'شاركنا برايك',
        'address' => 'العنوان',
        'facebook' => 'فيسبوك',
        'twitter' => 'تويتر',
        'google' => 'جوجل',
        'instagram' => 'ايستجرام',
        'copywrights' => 'حقوق الملكيه',
        'commonquestion' => 'الأسئله العامه',
        'orderquestions' => 'الاسئله الخاصه بالطلب',
        'organizationalstructure' => 'الهيكل التنظيمي',
        'technecialquestion' => 'الاسئله التقنيه',
        'relatedwords' => 'الكلمات الدلالية',
        'details' => 'تفاصيل',
        'security' => 'خصوصية الموقع',
        'roles' => 'شروط الاستخدام',
        'summary' => 'نبذه عن التطبيق',
        'start_time' => 'وقت بداية العمل',
        'end_time' => 'وقت نهاية العمل',
        'page_settings' => 'خصائص الصفحات الثابته',
        'logo' => 'صورة الموقع',

    ],

];
