{{ Form::bsSelect(trans('clients.singular'), 'client_id', $clients, null, ['autofocus','required', $readonly ?? '']) }}
{{ Form::bsNumber(trans('installments.attributes.installment_count'), 'count', null, ['required', $readonly ?? '']) }}
{{ Form::bsNumber(trans('installments.attributes.installment'), 'cost', null, ['required', $readonly ?? '']) }}
{{ Form::bsSelect(trans('installments.attributes.bill'), 'bill_id', $bills, null, [ $readonly ?? '']) }}
{{ Form::bsDate(trans('installments.attributes.start_date'), 'start_date', null, ['required', $readonly ?? '']) }}

{{ Form::bsDate(trans('installments.attributes.end_date'), 'end_date', null, ['required', $readonly ?? ''])  }}

<input type="hidden" name="product_id" value="{{$product->id}}">
<div class="form-group row">
    <div class="col-lg-12">
        {{ Form::bsSubmit($submitLabel ?? trans('forms.save'), ' icon-paperplane') }}
    </div>
</div>



