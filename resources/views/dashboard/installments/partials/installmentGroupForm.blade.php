@php($title = trans('installments.installment') . ' : ' . $product->name)

@extends('dashboard.layouts.main', ['title' => $title])

@section('content')


    @component('dashboard.components.page', ['header_title' => $title])

        @slot('breadcrumb')
            <li>
                <a href="{{ dashboard_route('installments.index') }}">
                    @lang('installments.plural')
                </a>
            </li>

            <li class="active">
                {{ $title }}
            </li>
        @endslot
        <div class="col-sm-12">
            <div class="panel border-left-lg border-left-success">
                <div class="panel-body" style="height:400px">

                    <div class="col-sm-12">
                        <div class="panel-heading"
                             style="background-color: #edf9f8;    padding-bottom: 2px;padding-top: 2px;border-radius: 8px;margin-bottom: 4px;">
                            <h5 class="no-margin-top">
                                <a href="{{ dashboard_route('products.show', $product) }}">{{ $product['name'] }}</a>
                            </h5>
                        </div>
                        <p class="mb-15" style="font-size: 17px">
                            <i class=" icon-price-tag"
                               style="color: #263238"></i> @lang('products.attributes.sector')
                            : {{  $product->sector }}
                        </p>
                        <p class="mb-15" style="font-size: 17px">
                            <i class="  icon-list-numbered"
                               style="color: {{ $product->count > 0 ? "#7EFC7E" : 'red'}}"></i> @lang('products.attributes.count')
                            : {{  $product->count }}
                        </p>

                        <p class="mb-15"><i class=" icon-magazine"
                                            style="color: #e8aee3;font-size: 23px"></i> {{  $product->description  }}
                        </p>
                        <p>
                            {{ Form::open(['route' => ['dashboard.installments.storeGroup', $product], 'method' => 'post', 'class' => 'form-horizontal']) }}
                            {{ Form::bsSelect2(trans('installments.attributes.group'), 'group_id', $groups, null, [ 'required',$readonly ?? '']) }}
                            {{ Form::bsNumber(trans('installments.attributes.installment_count'), 'count', null, ['required', $readonly ?? '']) }}
                            {{ Form::bsNumber(trans('installments.attributes.installment'), 'cost', null, ['required', $readonly ?? '']) }}
                            <input type="hidden" name="product_id" value="{{$product->id}}">

                        <div class="form-group row">
                            <div class="col-lg-12">
                                {{ Form::bsSubmit($submitLabel ?? trans('forms.save'), ' icon-paperplane') }}
                            </div>
                        </div>
                        {{ Form::close() }}
                        </p>
                        <div style="min-height:8px;max-height: 10px " class="col-sm-12">
                            <a href={{ dashboard_route('products.show', $product) }} title='{{ $product->name }}'></a>
                        </div>
                    </div>
                </div>

                <div class="panel-footer panel-footer-condensed" style="max-height:37px;">

                    <div class="heading-elements">

                        <i class="icon-pin" style="color: mediumvioletred"></i>
                        <span class="text-semibold"> {{ $product->store['name'] }}</span>


                    </div>
                </div>
            </div>
        </div>

    @endcomponent



@endsection