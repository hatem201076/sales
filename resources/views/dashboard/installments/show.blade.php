@php($title = trans('installments.singular') . ' : ' . $installment->product->name)

@extends('dashboard.layouts.main', ['title' => $title])

@section('content')

    @component('dashboard.components.page', ['header_title' => $title])

        @slot('breadcrumb')
            <li>
                <a href="{{ dashboard_route('installments.index') }}">
                    @lang('installments.plural')
                </a>
            </li>

            <li class="active">
                {{ $title }}
            </li>
        @endslot

        <div class="col-md-12">
            <div class="panel border-left-lg border-left-success">
                <div class="panel-body" style="height:550px">

                    <div class="col-xs-12">
                        <div class="panel-heading"
                             style="background-color: #edf9f8; padding-bottom: 2px;padding-top: 2px;border-radius: 8px;margin-bottom: 4px;">
                            <h5>
                                <a href="{{ dashboard_route('products.show', $installment->product) }}">{{ $installment->product->name }}</a>
                            </h5>
                        </div>
                        <p class="mb-15" style="font-size: 17px">

                            <i class=" icon-price-tag"
                               style="color: #263238"></i> @lang('installments.attributes.installment')
                            : {{  $installment->all }}
                        </p>

                        <p class="mb-15" style="font-size: 17px">

                            <i class="  icon-list-numbered"
                               style="color: #7EFC7E  "></i> @lang('installments.attributes.installment_count')

                            : {{  $installment->count }}
                        </p>

                        <p class="mb-15" style="font-size: 17px">
                            <i class="  icon-watch2" style="color: #e8aee3;font-size: 23px">

                            </i>
                            @lang('installments.attributes.start_date')
                            : {{  $installment->start_date }}
                        </p>

                        <p class="mb-15" style="font-size: 17px">
                            <i class="  icon-watch2"
                               style="color: #1d75b3;font-size: 23px"></i> @lang('installments.attributes.end_date')
                            : {{  $installment->end_date }}
                        </p>

                        <p class="mb-15" style="font-size: 17px">
                            <i class="  icon-man"
                               style="color: #4A148C;font-size: 23px"></i> @lang('installments.attributes.saler')
                            : {{  $installment->user['name'] }}
                        </p>

                        <p class="mb-15" style="font-size: 17px">
                            <i class="  icon-man"
                               style="color: #7E57C2;font-size: 23px"></i> @lang('installments.attributes.client_id')
                            : {{  $installment->client['name'] }}
                        </p>

                        <p class="mb-15" style="font-size: 17px">
                            <i class="text_info icon-cash3"
                               style="font-size: 23px"></i> @lang('installments.attributes.cost')
                            : {{ $cost }}
                            @if($cost === null) <span class="label label-danger">{{ $message }}</span>@endif

                        </p>


                        <p class="mb-15 " style="font-size: 17px">
                            <i class="   icon-pie-chart6"
                               style="color: #7fca83;font-size: 23px"></i> @lang('installments.attributes.cost_count')
                            : {{   $count_monthes }}
                            @if($count_monthes === null) <span class="label label-danger">{{ $message }}</span>@endif
                        </p>

                        <p class="mb-15" style="font-size: 17px">
                            <i class="   icon-cash3 text-success"
                               style="font-size: 23px"></i> @lang('installments.attributes.collected')
                            @if($sum >0)
                            : {{ $sum }}
                                <a href="{{ dashboard_route('collecteds.index', $installment) }}">
                                    <button class="btn btn-info">{{ trans('collecteds.info') }}</button>
                                </a>
                            @else
                                <span class="text-danger">: {{ trans('collecteds.empty') }}</span>
                            @endif
                            <a href="{{ dashboard_route('collecteds.create', $installment) }}" class="btn btn-primary">
                                @lang('collecteds.actions.add')
                                <i class="icon-plus3"></i>
                            </a>
                        </p>

                    </div>
                </div>

                <div class="panel-footer panel-footer-condensed" style="max-height:37px;">

                    <div class="heading-elements">
                        @php($product = $installment->product)
                        <span class="heading-text" title="{{ $product->name }}">
                    <i class="icon-pin" style="color: mediumvioletred"></i>
                    <span class="text-semibold"> {{ $product->store['name'] }}</span>
                </span>

                        <ul class="list-inline list-inline-condensed heading-text pull-right">

                            <li class="dropdown">
                                <a href="#" class="text-default dropdown-toggle" data-toggle="dropdown">
                                    <i class="icon-menu7"></i> <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    @can('update', $installment)
                                        <li>
                                            <a
                                                    href="{{ dashboard_route('installments.edit',[ $installment, $product]) }}"
                                                    class="btn btn-default text-info btn-xs"
                                            >
                                                @lang('forms.edit')
                                                <i class="icon-pencil7"></i>
                                            </a>
                                            @endcan
                                            @can('delete', $installment)
                                                <a
                                                        href="javascript:void(0)"
                                                        class="delete-confirm btn btn-default text-danger btn-xs"
                                                        data-url="{{ dashboard_route('installments.destroy', $installment) }}"
                                                        data-title="@lang('installments.dialogs.delete.title', ['item' => $installment->product['name']])"
                                                        data-message="@lang('installments.dialogs.delete.info')"
                                                >
                                                    @lang('forms.delete')
                                                    <i class="icon-trash"></i>
                                                </a>
                                        </li>
                                    @endcan

                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    @endcomponent



@endsection