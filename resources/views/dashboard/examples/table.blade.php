@extends('dashboard.layouts.main')

@section('content')

    @component('dashboard.components.page', ['header_title' => 'Page Title'])

        @slot('breadcrumb')
            <li>
                <a href="#">
                    Examples
                </a>
            </li>
            <li>
                <a href="#">
                    Table
                </a>
            </li>
        @endslot

        @slot('header_elements')
            <a href="#" class="btn btn-primary">
                Add Resource
            </a>
        @endslot

        @component('dashboard.components.list', ['heading' => 'List of resources'])
            @slot('thead')
                <tr>
                    <th>Field 1</th>
                    <th>Field 2</th>
                    <th>Field 3</th>
                </tr>
            @endslot

            @slot('tbody')
                <tr>
                    <td>Data 1</td>
                    <td>Data 2</td>
                    <td>Data 3</td>
                </tr>
                <tr>
                    <td>Data 1</td>
                    <td>Data 2</td>
                    <td>Data 3</td>
                </tr>
                <tr>
                    <td>Data 1</td>
                    <td>Data 2</td>
                    <td>Data 3</td>
                </tr>
            @endslot

        @endcomponent
    @endcomponent

@endsection