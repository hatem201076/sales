@php($title = trans('collecteds.plural'). ' : ' . $installment->product->name )

@extends('dashboard.layouts.main', ['title' => $title])

@section('content')

    @component('dashboard.components.page', ['header_title' => $title])

        @slot('breadcrumb')
            <li class="active">
                <a href="{{ route('dashboard.installments.show', $installment) }}">{{ $title }}</a>
            </li>
        @endslot

        <a href="{{ route('dashboard.installments.show', $installment) }}" style="font-size: 17px"> مشاهدة قسط  {{ $installment->product->name }}  </a>

        @component('dashboard.components.list', ['model' => $collecteds])
            @slot('thead')
                <th style="font-size: 16px;font-weight: bold;">@lang('collecteds.attributes.new_collected')</th>
                <th style="font-size: 16px;font-weight: bold;">@lang('collecteds.attributes.date')</th>
                <th style="font-size: 16px;font-weight: bold;">@lang('collecteds.attributes.created_at')</th>
                <th style="font-size: 16px;font-weight: bold;">...</th>
            @endslot


            @slot('tbody')

                @foreach ($collecteds as $collected)

                    <tr>

                        <td>

                            {{ $collected->collected }}

                        </td>

                        <td>

                            {{ $collected->date }}

                        </td>

                        <td>
                            {{ $collected->created_at->format('Y-m-d H:i') }}
                        </td>


                        <td>

                                <a
                                        href="{{ dashboard_route('collecteds.edit', [$collected, $installment]) }}"
                                        class="btn btn-default text-info btn-xs"
                                >
                                    @lang('forms.edit')
                                    <i class="icon-pencil7"></i>
                                </a>

                                <a
                                        href="javascript:void(0)"
                                        class="delete-confirm btn btn-default text-danger btn-xs"
                                        data-url="{{ dashboard_route('collecteds.destroy', $collected) }}"
                                        data-title="@lang('collecteds.dialogs.delete.title', ['item' => $collected->installment->product->name])"
                                        data-message="@lang('collecteds.dialogs.delete.info')"
                                >
                                    @lang('forms.delete')
                                    <i class="icon-trash"></i>
                                </a>

                        </td>
                    </tr>
                @endforeach

            @endslot

        @endcomponent

    @endcomponent

@endsection
