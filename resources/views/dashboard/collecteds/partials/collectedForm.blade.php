{{ Form::bsNumber(trans('collecteds.attributes.new_collected'), 'collected', null, [ $readonly ?? '']) }}

{{ Form::bsDate(trans('collecteds.attributes.date'), 'date', null, ['required', $readonly ?? '']) }}

<input type="hidden" name="installment_id" value="{{$installment->id}}">
<div class="form-group row">
    <div class="col-lg-12">
        {{ Form::bsSubmit($submitLabel ?? trans('forms.save'), ' icon-paperplane') }}
    </div>
</div>