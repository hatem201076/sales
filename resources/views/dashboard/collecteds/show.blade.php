@php($title = trans('collecteds.singular') . ' : ' . $installment->product->name)

@extends('dashboard.layouts.main', ['title' => $title])

@section('content')

    @component('dashboard.components.page', ['header_title' => $title])

        @slot('breadcrumb')
            <li>
                <a href="{{ dashboard_route('collecteds.index') }}">
                    @lang('collecteds.plural')
                </a>
            </li>

            <li class="active">
                {{ $title }}
            </li>
        @endslot

        @slot('header_elements')
            <a href="{{ dashboard_route('collecteds.create', $installment) }}" class="btn btn-primary">
                @lang('collecteds.actions.add')
                <i class="icon-plus3"></i>
            </a>
        @endslot

        <div class="col-md-12">
            <div class="panel border-left-lg border-left-success">
                <div class="panel-body" style="height:550px">

                    <div class="col-xs-12">
                        <div class="panel-heading"
                             style="background-color: #edf9f8; padding-bottom: 2px;padding-top: 2px;border-radius: 8px;margin-bottom: 4px;">
                            <h5>
                                <a href="{{ dashboard_route('collecteds.show', $collected->installment->product->name) }}">{{ $installment->product->name }}</a>
                            </h5>
                        </div>
                        <p class="mb-15" style="font-size: 17px">

                            <i class=" icon-price-tag"
                               style="color: #263238"></i> @lang('collecteds.attributes.collected')
                            : {{  $collected->collected }}
                        </p>

                        <p class="mb-15" style="font-size: 17px">

                            <i class="  icon-list-numbered"
                               style="color: #7EFC7E  "></i> @lang('installments.attributes.installment_count')

                            : {{  $collected->date }}
                        </p>


                    </div>
                </div>

                <div class="panel-footer panel-footer-condensed" style="max-height:37px;">

                    <div class="heading-elements">
                        @php($product = $installment->product)
                        <span class="heading-text" title="{{ $product->name }}">
                    <i class="icon-pin" style="color: mediumvioletred"></i>
                    <span class="text-semibold"> {{ $product->store['name'] }}</span>
                </span>

                        <ul class="list-inline list-inline-condensed heading-text pull-right">

                            <li class="dropdown">
                                <a href="#" class="text-default dropdown-toggle" data-toggle="dropdown">
                                    <i class="icon-menu7"></i> <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    @can('update', $installment)
                                        <li>
                                            <a
                                                    href="{{ dashboard_route('installments.edit',[ $installment, $product]) }}"
                                                    class="btn btn-default text-info btn-xs"
                                            >
                                                @lang('forms.edit')
                                                <i class="icon-pencil7"></i>
                                            </a>
                                            @endcan
                                            @can('delete', $installment)
                                                <a
                                                        href="javascript:void(0)"
                                                        class="delete-confirm btn btn-default text-danger btn-xs"
                                                        data-url="{{ dashboard_route('installments.destroy', $installment) }}"
                                                        data-title="@lang('installments.dialogs.delete.title', ['item' => $installment->product['name']])"
                                                        data-message="@lang('installments.dialogs.delete.info')"
                                                >
                                                    @lang('forms.delete')
                                                    <i class="icon-trash"></i>
                                                </a>
                                        </li>
                                    @endcan

                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    @endcomponent



@endsection