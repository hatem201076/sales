@php($title = trans('collecteds.collected') . ' : ' . $installment->product->name)

@extends('dashboard.layouts.main', ['title' => $title])

@section('content')


    @component('dashboard.components.page', ['header_title' => $title])

        @slot('breadcrumb')
            <li>
                <a href="{{ dashboard_route('collecteds.index', $installment) }}">
                    @lang('installments.plural')
                </a>
            </li>

            <li class="active">
                {{ $title }}
            </li>
        @endslot
        <div class="col-md-12">
            <div class="panel border-left-lg border-left-success">
                <div class="panel-body" style="height:550px">

                    <div class="col-xs-12">
                        <div class="panel-heading"
                             style="background-color: #ffefef;    padding-bottom: 2px;padding-top: 2px;border-radius: 8px;margin-bottom: 4px;">
                            <h5 class="no-margin-top">
                                <a href="{{ dashboard_route('installments.show', $installment) }}">{{ $installment->product['name'] }}</a>
                            </h5>
                        </div>
                        <p class="mb-15" style="font-size: 17px">

                            <i class=" icon-price-tag"
                               style="color: #263238"></i> @lang('installments.attributes.installment')
                            : {{  $installment->cost }}
                        </p>

                        <p class="mb-15" style="font-size: 17px">

                            <i class="  icon-list-numbered"
                               style="color: #7EFC7E  "></i> @lang('installments.attributes.installment_count')

                            : {{  $installment->count }}
                        </p>

                        <p class="mb-15" style="font-size: 17px">
                            <i class="  icon-watch2" style="color: #e8aee3;font-size: 23px">

                            </i>
                            @lang('installments.attributes.start_date')
                            : {{  $installment->start_date }}
                        </p>

                        <p class="mb-15" style="font-size: 17px">
                            <i class="  icon-watch2"
                               style="color: #1d75b3;font-size: 23px"></i> @lang('installments.attributes.end_date')
                            : {{  $installment->end_date }}
                        </p>

                        <p class="mb-15" style="font-size: 17px">
                            <i class="  icon-man"
                               style="color: #4A148C;font-size: 23px"></i> @lang('installments.attributes.saler')
                            : {{  $installment->user['name'] }}
                        </p>

                        <p class="mb-15" style="font-size: 17px">
                            <i class="  icon-man"
                               style="color: #7E57C2;font-size: 23px"></i> @lang('installments.attributes.client_id')
                            : {{  $installment->client['name'] }}
                        </p>

                        <p class="mb-15" style="font-size: 17px">
                            <i class="   icon-cash3 text-success"
                               style="font-size: 23px"></i> @lang('installments.attributes.collected')
                            : {{ $sum = $installment->collecteds->sum('collected') }}
                            @if($sum = 0) <span class="text-danger">لا يوجد تحصيل بعد</span>@endif
                        </p>

                        <p>
                            {{ Form::open(['route' => ['dashboard.collecteds.store', $installment], 'method' => 'POST', 'class' => 'form-horizontal']) }}
                            @include('dashboard.collecteds.partials.collectedForm', ['submitLabel' => trans('forms.save')])
                            {{ Form::close() }}
                        </p>

                    </div>
                </div>

            </div>
        </div>

    @endcomponent



@endsection