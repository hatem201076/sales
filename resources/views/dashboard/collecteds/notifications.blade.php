@php($title = trans('installments.notifications'))

@extends('dashboard.layouts.main', ['title' => $title])

@section('content')

    @component('dashboard.components.page', ['header_title' => $title])

        @slot('breadcrumb')
            <li class="active">
                {{ $title }}
            </li>
        @endslot

        <h4>الأقساط</h4>

        @component('dashboard.components.list')
            @slot('thead')
                <th style="font-size: 16px;font-weight: bold;">@lang('installments.attributes.name')</th>
                <th style="font-size: 16px;font-weight: bold;">@lang('installments.attributes.saler')</th>
                <th style="font-size: 16px;font-weight: bold;">@lang('installments.attributes.for')</th>

             @endslot


            @slot('tbody')


                @foreach($notifictions as $notification)
                    <tr>
                        <td>
                            <a href="{{ dashboard_route('installments.show', ['id' => $notification->data['id'], 'notify_id' => $notification->id]) }}"><span
                                        class="text-semibold">{{ $notification->data['installment'] }}</span></a>
                        </td>

                        <td>
                            <span class="text-semibold text-success">{{ $notification->data['client'] }}</span>

                        </td>

                        <td>
                            <span class="media-annotation">{{ $notification->created_at->diffForHumans() }}</span>

                        </td>

                    </tr>
                @endforeach

            @endslot

        @endcomponent

    @endcomponent

@endsection
