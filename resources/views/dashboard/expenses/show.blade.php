@php($title = trans('expenses.singular').' - '.$expense->name)

@extends('dashboard.layouts.main', ['title' => $title])

@section('content')

    @component('dashboard.components.page', ['header_title' => $title])

        @slot('breadcrumb')
            <li>
                <a href="{{ dashboard_route('expenses.index') }}">
                    @lang('expenses.plural')
                </a>
            </li>

            <li>
                {{ $title }}
            </li>
        @endslot

        @component('dashboard.components.panel', [
            'heading' => $title,
            'mode' => 'primary',
        ])

            {{ Form::model($expense, ['route' => ['dashboard.expenses.update', $expense], 'method' => 'patch', 'class' => 'form-horizontal', 'files' => true]) }}

            {{ Form::bsText(trans('expenses.attributes.name'), 'name', $expense->name, ['autofocus', 'required', 'readonly']) }}

            {{ Form::bsText(trans('expenses.attributes.date'), 'date', $expense->date, ['required', 'readonly']) }}

            {{ Form::bsText(trans('expenses.attributes.date'), 'amount', $expense->amount, ['autofocus', 'required', 'readonly'])  }}

            {{ Form::close() }}
            @can('update', $expense)
                <a
                        href="{{ dashboard_route('expenses.edit', $expense) }}"
                        class="btn btn-default text-info btn-xs"
                >
                    @lang('forms.edit')
                    <i class="icon-pencil7"></i>
                </a>
            @endcan
            @can('delete', $expense)
                <a
                        href="javascript:void(0)"
                        class="delete-confirm btn btn-default text-danger btn-xs"
                        data-url="{{ dashboard_route('expenses.destroy', $expense) }}"
                        data-title="@lang('expenses.dialogs.delete.title', ['item' => $expense->name])"
                        data-message="@lang('expenses.dialogs.delete.info')"
                >
                    @lang('forms.delete')
                    <i class="icon-trash"></i>
                </a>
            @endcan

        @endcomponent

    @endcomponent

@endsection
