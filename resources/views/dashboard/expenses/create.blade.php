@php($title = trans('expenses.actions.add'))

@extends('dashboard.layouts.main', ['title' => $title])

@section('content')

    @component('dashboard.components.page', ['header_title' => $title])

        @slot('breadcrumb')
            <li>
                <a href="{{ dashboard_route('expenses.index') }}">
                    @lang('expenses.plural')
                </a>
            </li>

            <li class="active">
                {{ $title }}
            </li>
        @endslot

        @component('dashboard.components.panel', [
            'heading' => trans('expenses.info'),
            'mode' => 'primary',
        ])

            {{ Form::open(['route' => ['dashboard.expenses.store'], 'class' => 'form-horizontal']) }}

            @include('dashboard.expenses.partials.form', ['submitLabel' => trans('forms.add')])

            {{ Form::close() }}

        @endcomponent

    @endcomponent

@endsection
