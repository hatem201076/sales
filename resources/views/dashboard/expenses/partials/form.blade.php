{{ Form::bsText(trans('expenses.attributes.name'), 'name', null, ['autofocus', 'required', $readonly ?? '']) }}
{{ Form::bsDate(trans('expenses.attributes.date'), 'date', null, ['required', $readonly ?? '']) }}
{{ Form::bsNumber(trans('expenses.attributes.amount'), 'amount', null, ['required', $readonly ?? '']) }}
<div class="form-group row">
    <div class="col-lg-12">
        {{ Form::bsSubmit($submitLabel ?? trans('forms.edit'), ' icon-paperplane') }}
    </div>
</div>



