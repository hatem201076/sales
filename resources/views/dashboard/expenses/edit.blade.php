@php($title = trans('expenses.singular') . ' : ' . $expense->name)

@extends('dashboard.layouts.main', ['title' => $title])

@section('content')

    @component('dashboard.components.page', ['header_title' => $title])

        @slot('breadcrumb')
            <li>
                <a href="{{ dashboard_route('expenses.index') }}">
                    @lang('expenses.plural')
                </a>
            </li>

            <li class="active">
                {{ $title }}
            </li>
        @endslot

        @component('dashboard.components.panel', [
            'heading' => trans('expenses.info'),
            'mode' => 'primary',
        ])

            {{ Form::model( $expense, ['route' => ['dashboard.expenses.update', $expense], 'method' => 'put', 'class' => 'form-horizontal', 'files' => true]) }}

            @include('dashboard.expenses.partials.form', ['submitLabel' => trans('forms.edit')])

            {{ Form::close() }}

        @endcomponent

    @endcomponent

@endsection
