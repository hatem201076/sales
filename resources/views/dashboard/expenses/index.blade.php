@php($title = trans('expenses.plural'))

@extends('dashboard.layouts.main', ['title' => $title])

@section('content')

    @component('dashboard.components.page', ['header_title' => $title])

        @slot('breadcrumb')
            <li class="active">
                {{ $title }}
            </li>
        @endslot

        @can('create', \App\Models\Expense::class)
            @slot('header_elements')
                <a href="{{ dashboard_route('expenses.create') }}" class="btn btn-primary">
                    @lang('expenses.actions.add')
                    <i class="icon-plus3"></i>
                </a>
            @endslot
        @endcan
        @component('dashboard.components.list', ['model' => $expenses ])
            @slot('thead')
                <th style="font-size: 16px;font-weight: bold;">@lang('expenses.attributes.name')</th>
                <th style="font-size: 16px;font-weight: bold;">@lang('expenses.attributes.date')</th>
                <th style="font-size: 16px;font-weight: bold;">@lang('expenses.attributes.amount')</th>
                <th style="font-size: 16px;font-weight: bold;">...</th>
            @endslot


            @slot('tbody')

                @foreach ($expenses as $expens )

                    <tr>
                        <td>
                            <a href="{{ dashboard_route('expenses.show', $expens) }}"
                               style="font-size: 19px; color: #26a69a;">
                                {{ $expens->name }}
                            </a>
                        </td>
                        <td>

                                {{ $expens->date }}

                        </td>
                        <td>
                                {{ $expens->amount }}
                        </td>


                        <td class=" table-actions">
                            @can('update', $expens)
                                <a
                                        href="{{ route('dashboard.expenses.edit', $expens) }}"
                                        class="btn btn-default text-info btn-xs"
                                >
                                    @lang('forms.edit')
                                    <i class="icon-pencil7"></i>
                                </a>
                            @endcan
                            @can('delete', $expens)
                                <a
                                        href="javascript:void(0)"
                                        class="delete-confirm btn btn-default text-danger btn-xs"
                                        data-url="{{ dashboard_route('expenses.destroy', $expens) }}"
                                        data-title="@lang('expenses.dialogs.delete.title', ['item' => $expens->name])"
                                        data-message="@lang('expenses.dialogs.delete.info')"
                                >
                                    @lang('forms.delete')
                                    <i class="icon-trash"></i>
                                </a>
                            @endcan
                        </td>
                    </tr>
                @endforeach

            @endslot

        @endcomponent

    @endcomponent

@endsection
