@php($title = trans('bills.actions.add'))

@extends('dashboard.layouts.main', ['title' => $title])

@section('content')

    @component('dashboard.components.page', ['header_title' => $title])

        @slot('breadcrumb')
            <li>
                <a href="{{ dashboard_route('bills.index') }}">
                    @lang('bills.plural')
                </a>
            </li>

            <li class="active">
                {{ $title }}
            </li>
        @endslot

        @component('dashboard.components.panel', [
            'heading' => trans('bills.info'),
            'mode' => 'primary',
        ])

            {{ Form::model( $bill, ['route' => ['dashboard.bills.update', $bill], 'method' => 'put', 'class' => 'form-horizontal', 'files' => true]) }}

            @include('dashboard.bills.partials.form', ['submitLabel' => trans('forms.update')])

            {{ Form::close() }}

        @endcomponent

    @endcomponent

@endsection
