@php($title = trans('bills.actions.add'))

@extends('dashboard.layouts.main', ['title' => $title])

@section('content')

    @component('dashboard.components.page', ['header_title' => $title])

        @slot('breadcrumb')
            <li>
                <a href="{{ dashboard_route('bills.index') }}">
                    @lang('bills.plural')
                </a>
            </li>

            <li class="active">
                {{ $title }}
            </li>
        @endslot

        @component('dashboard.components.panel', [
            'heading' => trans('bills.info'),
            'mode' => 'primary',
        ])

            {{ Form::open(['route' => ['dashboard.bills.store'], 'class' => 'form-horizontal']) }}

            @include('dashboard.bills.partials.form', ['submitLabel' => trans('forms.add')])

            {{ Form::close() }}

        @endcomponent

    @endcomponent

@endsection
