{{ Form::bsText(trans('bills.attributes.name'), 'name', null, ['autofocus', 'required', $readonly ?? '']) }}
{{ Form::bsSelect2(trans('bills.attributes.type'), 'type',  trans('bills.type'), ['required', $readonly ?? '']) }}
<div class="form-group row">
    <div class="col-lg-12">
        {{ Form::bsSubmit($submitLabel ?? trans('forms.save'), ' icon-paperplane') }}
    </div>
</div>



