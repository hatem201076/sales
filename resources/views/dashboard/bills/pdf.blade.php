@extends('dashboard.reports.layout.pdf')

@section('content')


    <h2 class="pull-right">الصقر</h2>
    <h4> أجهزة كهربائية- أدوات منزلية - سجاد</h4>
    <h4> الحاج/ إبراهيم صقر وأولاده</h4>
    <hr width="5cm">
    <h4 class="text-center"> بيان أسعار </h4>
    <h6 class="pull-right">  تحريراً فى : {{ now() }} </h6>
    <h6 class="pull-right">  المطلوب من السيد : {{ $bill->name }} </h6>
    <h6 class="pull-right">   البائع : {{ auth()->user()->name }} </h6>



    <table class="table" align="center" width="700px" style="height: 700px; text-align: center">
        <tr>
            <th class="text-center">إسم الصنف</th>
            <th class="text-center">الوحدة</th>
            <th class="text-center">الكمية</th>
            <th class="text-center">الإجمالي</th>

        </tr>
        @foreach($installments as $installment)
            <tr>

                <td>{{ $installment->product['name'] }}</td>
                <td>{{ $installment->cost }}</td>
                <td>{{ $installment->count }}</td>
                <td>{{ $installment->all }}</td>
            </tr>
        @endforeach
        <tr>
            <td>مبلغ وقدره</td>
            <td> ... </td>
            <td> ... </td>
            <td>{{ $all }}</td>
        </tr>
    </table>

@endsection

