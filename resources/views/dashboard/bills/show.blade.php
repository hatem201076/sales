@php($title = trans('bills.singular') . ' : ' . $bill->name)
@extends('dashboard.layouts.main', ['title' => $title])

@section('content')

    @component('dashboard.components.page', ['header_title' => $title])

        @slot('breadcrumb')
            <li>
                <a href="{{ dashboard_route('bills.index') }}">
                    @lang('bills.plural')
                </a>
            </li>

            <li>
                {{ $title }}
            </li>
        @endslot

        @component('dashboard.components.list', ['model' => $installments])
            @slot('thead')
                <th style="font-size: 16px;font-weight: bold;">@lang('bills.attributes.name')</th>
                <th style="font-size: 16px;font-weight: bold;">@lang('installments.attributes.saler')</th>
                <th>...</th>
            @endslot

            {{--{{ dd($installments) }}--}}

            @slot('tbody')

                @foreach ($installments as $installment)
                    <tr>
                        <td>
                                {{ $installment->product['name'] }}

                        </td>

                        <td>

                            {{ $installment->user->name }}

                        </td>


                    </tr>

                @endforeach

                {{ Form::open(['route' => ['dashboard.bills.pdf', $bill], 'method' => 'GET', 'class' => 'form-horizontal']) }}
                {{ Form::bsSubmit('Pdf',' icon-paperplane') }}
                {{ Form::close() }}
            @endslot

        @endcomponent

    @endcomponent

@endsection
