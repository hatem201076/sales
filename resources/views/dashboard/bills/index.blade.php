@php($title = trans('bills.plural'))

@extends('dashboard.layouts.main', ['title' => $title])

@section('content')

    @component('dashboard.components.page', ['header_title' => $title])

        @slot('breadcrumb')
            <li class="active">
                {{ $title }}
            </li>
        @endslot

        @can('create', \App\Models\Bill::class)
            @slot('header_elements')
                <a href="{{ dashboard_route('bills.create') }}" class="btn btn-primary">
                    @lang('bills.actions.add')
                    <i class="icon-plus3"></i>
                </a>
            @endslot
        @endcan
        @component('dashboard.components.list', ['model' => $bills ])
            @slot('thead')
                <th style="font-size: 16px;font-weight: bold;">@lang('bills.attributes.name')</th>
                <th style="font-size: 16px;font-weight: bold;">...</th>
            @endslot


            @slot('tbody')

                @foreach ($bills as $bill )

                    <tr>
                        <td>
                            <a href="{{ dashboard_route('bills.pdf', $bill) }}"
                               style="font-size: 19px; color: #26a69a;">
                                {{ $bill->name }}
                            </a>
                        </td>


                        <td class=" table-actions">
                            @can('update', $bill)
                                <a
                                        href="{{ route('dashboard.bills.edit', $bill) }}"
                                        class="btn btn-default text-info btn-xs"
                                >
                                    @lang('forms.edit')
                                    <i class="icon-pencil7"></i>
                                </a>
                            @endcan
                            @can('delete', $bill)
                                <a
                                        href="javascript:void(0)"
                                        class="delete-confirm btn btn-default text-danger btn-xs"
                                        data-url="{{ dashboard_route('bills.destroy', $bill) }}"
                                        data-title="@lang('bills.dialogs.delete.title', ['item' => $bill->name])"
                                        data-message="@lang('bills.dialogs.delete.info')"
                                >
                                    @lang('forms.delete')
                                    <i class="icon-trash"></i>
                                </a>
                            @endcan
                        </td>
                    </tr>
                @endforeach

            @endslot

        @endcomponent

    @endcomponent

@endsection
