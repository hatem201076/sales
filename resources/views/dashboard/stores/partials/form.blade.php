{{ Form::bsText(trans('stores.attributes.name'), 'name', null, ['autofocus', 'required', $readonly ?? '']) }}

{{ Form::bsSubmit($submitLabel ?? trans('forms.save'), ' icon-paperplane') }}