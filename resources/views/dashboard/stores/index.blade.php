@php($title = trans('stores.plural'))

@extends('dashboard.layouts.main', ['title' => $title])

@section('content')

    @component('dashboard.components.page', ['header_title' => $title])

        @slot('breadcrumb')
            <li class="active">
                {{ $title }}
            </li>
        @endslot

        @can('create', \App\Models\Store::class)
            @slot('header_elements')
                <a href="{{ dashboard_route('stores.create') }}" class="btn btn-primary">
                    @lang('stores.actions.add')
                    <i class="icon-plus3"></i>
                </a>
            @endslot
        @endcan

        @component('dashboard.components.list', ['model' => $stores])
            @slot('thead')
                <th style="font-size: 16px;font-weight: bold;">@lang('stores.attributes.name')</th>
                <th style="font-size: 16px;font-weight: bold;">@lang('stores.products')</th>
                <th>...</th>
            @endslot


            @slot('tbody')

                @foreach ($stores as $store)

                    <tr>
                        <td>
                            <a href="{{ dashboard_route('stores.show', $store) }}" style="font-size: 19px; color: #26a69a;">
                                {{ $store->name }}
                            </a>
                        </td>
                        <td>
                            <a
                                    href="{{ dashboard_route('stores.show', $store) }}"
                                    class="btn btn-default text-success btn-xs"
                            >
                                @lang('stores.products')
                                <i class="icon-pencil7"></i>
                            </a>

                        </td>
                        <td class="table-actions">
                            @can('update', $store)
                                <a
                                        href="{{ dashboard_route('stores.edit', $store) }}"
                                        class="btn btn-default text-info btn-xs"
                                >
                                    @lang('forms.edit')
                                    <i class="icon-pencil7"></i>
                                </a>
                            @endcan
                            @can('delete', $store)
                                <a
                                        href="javascript:void(0)"
                                        class="delete-confirm btn btn-default text-danger btn-xs"
                                        data-url="{{ dashboard_route('stores.destroy', $store) }}"
                                        data-title="@lang('stores.dialogs.delete.title', ['item' => $store->name])"
                                        data-message="@lang('stores.dialogs.delete.info')"
                                >
                                    @lang('forms.delete')
                                    <i class="icon-trash"></i>
                                </a>
                            @endcan
                        </td>
                    </tr>
                @endforeach

            @endslot

        @endcomponent

    @endcomponent

@endsection
