@php($title = trans('stores.actions.add'))

@extends('dashboard.layouts.main', ['title' => $title])

@section('content')

    @component('dashboard.components.page', ['header_title' => $title])

        @slot('breadcrumb')
            <li>
                <a href="{{ dashboard_route('stores.index') }}">
                    @lang('stores.plural')
                </a>
            </li>

            <li class="active">
                {{ $title }}
            </li>
        @endslot

        @component('dashboard.components.panel', [
            'heading' => trans('stores.info'),
            'mode' => 'primary',
        ])

            {{ Form::open(['route' => ['dashboard.stores.store'], 'method' => 'post', 'class' => 'form-horizontal', 'files' => true]) }}

            @include('dashboard.stores.partials.form', ['submitLabel' => trans('forms.add')])

            {{ Form::close() }}

        @endcomponent

    @endcomponent

@endsection
