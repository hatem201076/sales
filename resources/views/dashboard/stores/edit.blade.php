@php($title = trans('stores.singular') . ' : ' . $store->name)

@extends('dashboard.layouts.main', ['title' => $title])

@section('content')

    @component('dashboard.components.page', ['header_title' => $title])

        @slot('breadcrumb')
            <li>
                <a href="{{ dashboard_route('stores.index') }}">
                    @lang('stores.plural')
                </a>
            </li>

            <li>
                {{ $title }}
            </li>
        @endslot

        @component('dashboard.components.panel', [
            'heading' => $title,
            'mode' => 'primary',
        ])

            {{ Form::model($store, ['route' => ['dashboard.stores.update', $store], 'method' => 'patch', 'class' => 'form-horizontal', 'files' => true]) }}

            @include('dashboard.stores.partials.form', ['submitLabel' => trans('forms.edit')])

            {{ Form::close() }}

        @endcomponent

    @endcomponent

@endsection
