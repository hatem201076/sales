{{ Form::bsText(trans('static_pages.attributes.title'), 'title', $static_page->title, ['autofocus', 'required']) }}

{{ Form::bsHtmlEditor(trans('static_pages.attributes.content'), 'content',  $static_page->content, ['required']) }}

{{ Form::bsSubmit($submitLabel ?? trans('forms.save'), ' icon-paperplane') }}