@php($title = trans('settings.singular'))

@extends('dashboard.layouts.main', ['title' => $title])

@section('content')

    @component('dashboard.components.page', ['header_title' => $title])

        @slot('breadcrumb')
            <li>
                <a href="{{ route('dashboard.settings.index') }}">
                    @lang('settings.plural')
                </a>
            </li>

            <li>
                {{ $title }}
            </li>
        @endslot

        @component('dashboard.components.panel', [
            'heading' => $title,
            'mode' => 'primary',
        ])

            {{ Form::open(['route' => ['dashboard.insert'], 'method' => 'post', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}

            {{ Form::bsText(trans('settings.attributes.title'), 'title',Setting::get('title'), ['class' => 'pickatime form-control']) }}

            {{ Form::bsText(trans('settings.attributes.address'), 'address', Setting::get('address')) }}

            {{ Form::bsText(trans('settings.attributes.phone'), 'phone', Setting::get('phone')) }}

            {{ Form::bsText(trans('settings.attributes.email'), 'email', Setting::get('email')) }}

            {{ Form::bsSubmit($submitLabel ?? trans('forms.save'), ' icon-paperplane') }}

            {{ Form::close() }}

        @endcomponent

    @endcomponent

@endsection

@push('scripts')
<script>
    $('.pickatime').pickatime()
</script>
@endpush

