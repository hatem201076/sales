@extends('dashboard.layouts.main')

@section('content')

    @component('dashboard.components.page', ['header_title' => trans('products.plural')])


        @if(auth()->user()->type != 0)
            @slot('header_elements')
                <a href="{{ dashboard_route('products.create') }}" class="btn btn-primary">
                    @lang('products.actions.add')
                    <i class="icon-plus3"></i>
                </a>
            @endslot
        @endif

        <div class="clearfix"></div>
        <div>
            {{ Form::open(['route' => ['dashboard.search'], 'method' => 'get', 'class' => 'form-horizontal form-group text-center  col-sm-6 col-sm-offset-3', 'files' => true]) }}

            <input type="text" class="form-control" name="text" placeholder="إبحث عن بضاعة بالإسم" autofocus>
            <input type="submit" class="btn btn-success" value="إبحــث">
            {{ Form::close() }}
        </div>
        <div class="clearfix"></div>

        @forelse ($products as $product)

            @include('dashboard.components.ProductCard')
        @empty
            <h3 class="text-center">{{ trans('products.empty') }}</h3>
        @endforelse
        <div class="text-center">  {{ $products->links() }} </div>


    @endcomponent



@endsection