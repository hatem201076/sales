@extends('dashboard.layouts.main')

@section('content')

    @component('dashboard.components.page', ['header_title' => trans('products.search.plural')])

        <div class="clearfix"></div>

        <div>
            {{ Form::open(['route' => ['dashboard.search'], 'method' => 'get', 'class' => 'form-horizontal form-group text-center  col-sm-6 col-sm-offset-3', 'files' => true]) }}

            <input type="text" class="form-control" name="text" placeholder="إبحث عن بضاعة بالإسم" >
            <input type="submit" class="btn btn-success" value="إبحــث">

            {{ Form::close() }}
        </div>

        <div class="clearfix"></div>


        @forelse ($products as $product)

            @include('dashboard.components.ProductCard')
        @empty
            <h3 class="text-center">{{ trans('products.search.empty') }}</h3>
        @endforelse
        <div class="text-center">  {{ $products->links() }} </div>


    @endcomponent



@endsection