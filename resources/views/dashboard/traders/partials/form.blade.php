{{ Form::bsText(trans('traders.attributes.name'), 'name', null, ['autofocus', 'required', $readonly ?? '']) }}
{{ Form::bsText(trans('traders.attributes.phone'), 'phone', null, ['autofocus', 'required', $readonly ?? '']) }}

{{ Form::bsSubmit($submitLabel ?? trans('forms.save'), ' icon-paperplane') }}