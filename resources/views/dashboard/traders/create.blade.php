@php($title = trans('traders.actions.add'))

@extends('dashboard.layouts.main', ['title' => $title])

@section('content')

    @component('dashboard.components.page', ['header_title' => $title])

        @slot('breadcrumb')
            <li>
                <a href="{{ dashboard_route('traders.index') }}">
                    @lang('traders.plural')
                </a>
            </li>

            <li class="active">
                {{ $title }}
            </li>
        @endslot

        @component('dashboard.components.panel', [
            'heading' => trans('traders.info'),
            'mode' => 'primary',
        ])

            {{ Form::open(['route' => ['dashboard.traders.store'], 'method' => 'post', 'class' => 'form-horizontal', 'files' => true]) }}

            @include('dashboard.traders.partials.form', ['submitLabel' => trans('forms.add')])

            {{ Form::close() }}

        @endcomponent

    @endcomponent

@endsection
