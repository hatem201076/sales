@php($title = trans('traders.plural'))

@extends('dashboard.layouts.main', ['title' => $title])

@section('content')

    @component('dashboard.components.page', ['header_title' => $title])

        @slot('breadcrumb')
            <li class="active">
                {{ $title }}
            </li>
        @endslot

        @can('create', \App\Models\Trader::class)
            @slot('header_elements')
                <a href="{{ dashboard_route('traders.create') }}" class="btn btn-primary">
                    @lang('traders.actions.add')
                    <i class="icon-plus3"></i>
                </a>
            @endslot
        @endcan
        @component('dashboard.components.list', ['model' => $traders])
            @slot('thead')
                <th style="font-size: 16px;font-weight: bold;">@lang('traders.attributes.name')</th>
                <th style="font-size: 16px;font-weight: bold;">@lang('traders.attributes.phone')</th>
                <th>...</th>
            @endslot


            @slot('tbody')

                @foreach ($traders as $trader)

                    <tr>
                        <td>
                            <a href="{{ dashboard_route('traders.show', $trader) }}"  style="font-size: 19px; color: #26a69a;">
                                {{ $trader->name }}
                            </a>
                        </td>
                        <td>
                                 {{ $trader->phone }}

                        </td>

                        <td class=" table-actions">
                            @can('update', $trader)
                                <a
                                        href="{{ dashboard_route('traders.edit', $trader) }}"
                                        class="btn btn-default text-info btn-xs"
                                >
                                    @lang('forms.edit')
                                    <i class="icon-pencil7"></i>
                                </a>
                            @endcan
                            @can('delete', $trader)
                                <a
                                        href="javascript:void(0)"
                                        class="delete-confirm btn btn-default text-danger btn-xs"
                                        data-url="{{ dashboard_route('traders.destroy', $trader) }}"
                                        data-title="@lang('traders.dialogs.delete.title', ['item' => $trader->name])"
                                        data-message="@lang('traders.dialogs.delete.info')"
                                >
                                    @lang('forms.delete')
                                    <i class="icon-trash"></i>
                                </a>
                            @endcan
                        </td>
                    </tr>
                @endforeach

            @endslot

        @endcomponent

    @endcomponent

@endsection
