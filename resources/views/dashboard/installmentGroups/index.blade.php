@php($title = trans('installmentGroups.plural'))

@extends('dashboard.layouts.main', ['title' => $title])

@section('content')

    @component('dashboard.components.page', ['header_title' => $title])

        @slot('breadcrumb')
            <li class="active">
                {{ $title }}
            </li>
        @endslot
        @slot('header_elements')
            <a href="{{ dashboard_route('installmentGroups.create') }}" class="btn btn-primary">
                أضف قسط مجمع
                <i class="icon-plus3"></i>
            </a>
        @endslot

        @component('dashboard.components.list', ['model' => $installments])
            @slot('thead')
                <th style="font-size: 16px;font-weight: bold;">@lang('installments.attributes.client_id')</th>
                <th style="font-size: 16px;font-weight: bold;">@lang('installments.attributes.start_date')</th>
                <th style="font-size: 16px;font-weight: bold;">@lang('installments.attributes.end_date')</th>
                <th style="font-size: 16px;font-weight: bold;">...</th>
                <th style="font-size: 16px;font-weight: bold;">...</th>
            @endslot


            @slot('tbody')

                @foreach ($installments as $installment)

                    <tr>


                        <td>

                            {{ $installment->client !=null ? $installment->client->name : 'لا يوجد عميل'}}

                        </td>

                        <td>
                            {{ $installment->start_date }}
                        </td>

                        <td>
                            {{ $installment->end_date  }}
                        </td>
                        <td>
                            <a href="{{ dashboard_route('installmentGroups.show', $installment) }}" class="text-success">
                                <strong> تفاصيل القسط <i class=" icon-eye4"></i></strong>
                            </a>
                        </td>

                        {{--<td>--}}
                            {{--@php($product = $installment->product)--}}
                                 {{--<a--}}
                                        {{--href="{{ dashboard_route('installmentGroups.edit',[ $installment, $product]) }}"--}}
                                        {{--class="btn btn-default text-info btn-xs"--}}
                                {{-->--}}
                                    {{--@lang('forms.edit')--}}
                                    {{--<i class="icon-pencil7"></i>--}}
                                {{--</a>--}}

                                 {{--<a--}}
                                        {{--href="javascript:void(0)"--}}
                                        {{--class="delete-confirm btn btn-default text-danger btn-xs"--}}
                                        {{--data-url="{{ dashboard_route('installmentGroups.destroy', $installment) }}"--}}
                                        {{--data-title="@lang('installments.dialogs.delete.title', ['item' => $installment->product['name']])"--}}
                                        {{--data-message="@lang('installments.dialogs.delete.info')"--}}
                                {{-->--}}
                                    {{--@lang('forms.delete')--}}
                                    {{--<i class="icon-trash"></i>--}}
                                {{--</a>--}}
                         {{--</td>--}}
                    </tr>
                @endforeach

            @endslot

        @endcomponent

    @endcomponent

@endsection
