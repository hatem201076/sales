@php($title = trans('installments.installment') . ' : ' . $product->name)

@extends('dashboard.layouts.main', ['title' => $title])

@section('content')


    @component('dashboard.components.page', ['header_title' => $title])

        @slot('breadcrumb')
            <li>
                <a href="{{ dashboard_route('installments.index') }}">
                    @lang('installments.plural')
                </a>
            </li>

            <li class="active">
                {{ $title }}
            </li>
        @endslot
        <div class="col-md-12">
            <div class="panel border-left-lg border-left-success">
                <div class="panel-body" style="height:800px">

                    <div class="col-xs-12">
                        <div class="panel-heading"
                             style="background-color: #edf9f8;    padding-bottom: 2px;padding-top: 2px;border-radius: 8px;margin-bottom: 4px;">
                            <h5 class="no-margin-top">
                                <a href="{{ dashboard_route('products.show', $product) }}">{{ $product['name'] }}</a>
                            </h5>
                        </div>
                        <p class="mb-15" style="font-size: 17px">
                            <i class=" icon-price-tag"
                               style="color: #263238"></i> @lang('products.attributes.sector')
                            : {{  $product->sector }}
                        </p>
                        <p class="mb-15" style="font-size: 17px">
                            <i class="  icon-list-numbered"
                               style="color: {{ $product->count > 0 ? "#7EFC7E" : 'red'}}"></i> @lang('products.attributes.count')
                            : {{  $product->count }}
                        </p>

                        <p class="mb-15"><i class=" icon-magazine"
                                            style="color: #e8aee3;font-size: 23px"></i> {{  $product->description  }}
                        </p>
                        <p>
                            {{ Form::model($installment, ['route' => ['dashboard.installments.update', $installment, $product], 'method' => 'PUT', 'class' => 'form-horizontal', 'files' => true]) }}
                            @include('dashboard.installments.partials.installmentForm', ['submitLabel' => trans('forms.edit')])
                            {{ Form::close() }}
                        </p>
                        <div style="min-height:8px;max-height: 10px " class="col-sm-12">
                            <a href={{ dashboard_route('products.show', $product) }} title='{{ $product->name }}'></a>
                        </div>
                    </div>
                </div>

                <div class="panel-footer panel-footer-condensed" style="max-height:37px;">

                    <div class="heading-elements">

                        <i class="icon-pin" style="color: mediumvioletred"></i>
                        <span class="text-semibold"> {{ $product->store['name'] }}</span>




                    </div>
                </div>
            </div>
        </div>

    @endcomponent



@endsection