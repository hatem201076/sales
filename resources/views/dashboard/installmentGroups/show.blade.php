@php($title = trans('installmentGroups.plural') . ' : ' . $installmentGroup->name)

@extends('dashboard.layouts.main', ['title' => $title])

@section('content')

    @component('dashboard.components.page', ['header_title' => $title])
        @slot('breadcrumb')
            <li class="active">
                {{ $title }}
            </li>
        @endslot

        <div style="color: green; font-size: 20px; overflow: hidden">
            <span class="col-sm-5">تاريخ نهاية القسط : {{ $installmentGroup->start_date }}</span>
            <span class="col-sm-5"> تاريخ أول القسط : {{ $installmentGroup->start_date }}</span>
        </div>

        @component('dashboard.components.list', ['model' => $installments])
            @slot('thead')
                <th style="font-size: 16px;font-weight: bold;">@lang('installments.attributes.name')</th>
                <th style="font-size: 16px;font-weight: bold;">@lang('installments.attributes.saler')</th>
                <th style="font-size: 16px;font-weight: bold;">@lang('installments.attributes.client_id')</th>
                <th style="font-size: 16px;font-weight: bold;">@lang('installments.attributes.installment_count')</th>
                <th style="font-size: 16px;font-weight: bold;">@lang('installments.attributes.installment')</th>
                <th style="font-size: 16px;font-weight: bold;"> الإجمالي</th>
                <th style="font-size: 16px;font-weight: bold;">@lang('sales.attributes.date')</th>
                <th style="font-size: 16px;font-weight: bold;">...</th>
            @endslot


            @slot('tbody')

                @foreach ($installments as $installment)

                    <tr>
                        <td>
                            <a href="{{ dashboard_route('installments.show', $installment) }}"
                               style="font-size: 19px; color: #26a69a"
                               class="text-primary ">
                                @if($installment->product['name'])
                                    {{  $installment->product['name'] }}
                                @else
                                    <h5 class="text-danger"> تم حذف البضاعة</h5>
                                @endif

                            </a>
                        </td>

                        <td>

                            {{ $installment->user !=null ? $installment->user->name :'لا يوجد بائع '}}

                        </td>

                        <td>

                            {{ $installment->client !=null ? $installment->client->name : 'لا يوجد عميل'}}

                        </td>

                        <td>
                            {{ $installment->count }}
                        </td>

                        <td>
                            {{ $installment->cost }}
                        </td>

                        <td>
                            {{ $installment->all }}
                        </td>

                        <td>
                            {{ $installment->created_at->format('Y-m-d H:i') }}
                        </td>

                        <td>
                            @php($product = $installment->product)
                            @can('update', $installment)
                                <a
                                        href="{{ dashboard_route('installments.edit',[ $installment, $product]) }}"
                                        class="btn btn-default text-info btn-xs"
                                >
                                    @lang('forms.edit')
                                    <i class="icon-pencil7"></i>
                                </a>
                            @endcan
                            @can('delete', $installment)
                                <a
                                        href="javascript:void(0)"
                                        class="delete-confirm btn btn-default text-danger btn-xs"
                                        data-url="{{ dashboard_route('installments.destroy', $installment) }}"
                                        data-title="@lang('installments.dialogs.delete.title', ['item' => $installment->product['name']])"
                                        data-message="@lang('installments.dialogs.delete.info')"
                                >
                                    @lang('forms.delete')
                                    <i class="icon-trash"></i>
                                </a>
                            @endcan
                        </td>
                    </tr>
                @endforeach
                <tr style="font-size: 20px">
                    <td class="push-right">
                        الإجمالي
                    </td>
                    <td class="push-left">
                        قيمة القسط : {{ $installments->sum('all') }}
                    </td>
                </tr>
            @endslot

        @endcomponent

    @endcomponent

@endsection
