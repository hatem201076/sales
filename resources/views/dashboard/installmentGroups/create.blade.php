@php($title = trans('installmentGroups.installment'))

@extends('dashboard.layouts.main', ['title' => $title])

@section('content')


    @component('dashboard.components.page', ['header_title' => $title])

        @slot('breadcrumb')
            <li>
                <a href="{{ dashboard_route('installmentGroups.index') }}">
                    @lang('installmentGroups.plural')
                </a>
            </li>

            <li class="active">
                {{ $title }}
            </li>
        @endslot
        <div class="col-sm-12">
            <div class="panel border-left-lg border-left-success">
                <div class="panel-body" style="height:400px">

                    <div class="col-sm-12">

                        <p>
                            {{ Form::open(['route' => ['dashboard.installmentGroups.store'], 'method' => 'post', 'class' => 'form-horizontal']) }}
                            @include('dashboard.installmentGroups.partials.installmentGroupForm', ['submitLabel' => trans('forms.installment')])
                            {{ Form::close() }}
                        </p>

                    </div>
                </div>


            </div>
        </div>

    @endcomponent

@endsection