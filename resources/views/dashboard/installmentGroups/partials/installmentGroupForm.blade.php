{{ Form::bsText(trans('installmentGroups.attributes.name'), 'name', null, ['autofocus','required', $readonly ?? '']) }}
{{ Form::bsSelect(trans('clients.singular'), 'client_id', $clients, null, ['required', $readonly ?? '']) }}
{{ Form::bsSelect(trans('installments.attributes.bill'), 'bill_id', $bills, null, [ $readonly ?? '']) }}
{{ Form::bsDate(trans('installments.attributes.start_date'), 'start_date', null, ['required', $readonly ?? '']) }}
{{ Form::bsDate(trans('installments.attributes.end_date'), 'end_date', null, ['required', $readonly ?? ''])  }}

 <div class="form-group row">
    <div class="col-lg-12">
        {{ Form::bsSubmit($submitLabel ?? trans('forms.save'), ' icon-paperplane') }}
    </div>
</div>



