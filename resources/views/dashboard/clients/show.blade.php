@php($title = trans('clients.singular') . ' : ' . $client->name)

@extends('dashboard.layouts.main', ['title' => $title])

@section('content')

    @component('dashboard.components.page', ['header_title' => $title])

        @slot('breadcrumb')
            <li>
                <a href="{{ dashboard_route('clients.index') }}">
                    @lang('clients.plural')
                </a>
            </li>

            <li>
                {{ $title }}
            </li>

            <span class="text-success" style="padding: 20px;font-size: 20px;">
                @lang('clients.collected') : {{ $all }}
            </span>
            <span class="text-danger" style="padding: 20px;font-size: 20px;">
                @lang('clients.remainning') : {{ $remainning }}
            </span>
            {{--<span class="text-primary" style="padding: 20px;font-size: 20px;">--}}
                {{--@lang('products.attributes.wholesales') : {{ $wholesales }}--}}
            {{--</span>--}}
        @endslot

        @component('dashboard.components.list', ['model' => $installments])
            @slot('thead')
                <th style="font-size: 16px;font-weight: bold;">@lang('installments.attributes.name')</th>
                <th style="font-size: 16px;font-weight: bold;">@lang('installments.attributes.saler')</th>
                <th style="font-size: 16px;font-weight: bold;">@lang('installments.attributes.installment_count')</th>
                <th style="font-size: 16px;font-weight: bold;">@lang('installments.attributes.installment')</th>
                <th style="font-size: 16px;font-weight: bold;">@lang('installments.attributes.collected')</th>
                <th style="font-size: 16px;font-weight: bold;"> باقى </th>
                <th style="font-size: 16px;font-weight: bold;">@lang('sales.attributes.date')</th>
                <th style="font-size: 16px;font-weight: bold;">...</th>
            @endslot


            @slot('tbody')

                @foreach ($installments as $installment)

                    <tr>
                        <td>
                            <a href="{{ dashboard_route('installments.show', $installment) }}"
                               style="font-size: 19px; color: #26a69a"
                               class="text-primary ">
                                {{ $installment->product['name'] }}
                            </a>
                        </td>

                        <td>

                            {{ $installment->user->name }}

                        </td>

                        <td>
                            {{ $installment->count }}
                        </td>

                        <td>
                            {{ $installment->cost }}
                        </td>
                        <td>
                            @php($sum = $installment->collecteds->sum('collected'))
                            @if($sum >0)
                                {{ $sum }}
                                <a href="{{ dashboard_route('collecteds.index', $installment) }}">
                                    <button class="btn btn-info">{{ trans('collecteds.info') }}</button>
                                </a>
                            @else
                                <span class="text-danger"> {{ trans('collecteds.empty') }}</span>
                            @endif

                        </td>
                        @php($rem = $installment->all - $sum)
                        <td class=" @if($rem > 0) text-danger @else text-success @endif">
                            {{ $rem }}
                        </td>

                        <td>
                            {{ $installment->created_at->format('Y-m-d H:i') }}
                        </td>

                        <td>
                            @php($product = $installment->product)
                            @can('update', $installment)
                                <a href="{{ dashboard_route('collecteds.create', $installment) }}"
                                   class="btn btn-primary">
                                    @lang('collecteds.actions.add')
                                    <i class="icon-plus3"></i>
                                </a>
                                <a
                                        href="{{ dashboard_route('installments.edit',[ $installment, $product]) }}"
                                        class="btn btn-default text-info btn-xs"
                                >
                                    @lang('forms.edit')
                                    <i class="icon-pencil7"></i>
                                </a>
                            @endcan
                            @can('delete', $installment)
                                <a
                                        href="javascript:void(0)"
                                        class="delete-confirm btn btn-default text-danger btn-xs"
                                        data-url="{{ dashboard_route('installments.destroy', $installment) }}"
                                        data-title="@lang('installments.dialogs.delete.title', ['item' => $installment->product['name']])"
                                        data-message="@lang('installments.dialogs.delete.info')"
                                >
                                    @lang('forms.delete')
                                    <i class="icon-trash"></i>
                                </a>
                            @endcan
                        </td>
                    </tr>
                @endforeach

            @endslot

        @endcomponent

    @endcomponent

@endsection
