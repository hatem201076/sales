{{ Form::bsText(trans('clients.attributes.name'), 'name', null, ['autofocus', 'required', $readonly ?? '']) }}
{{ Form::bsText(trans('clients.attributes.phone'), 'phone', null, ['required', $readonly ?? '']) }}
<div class="form-group row">
    <div class="col-lg-12">
        {{ Form::bsSubmit($submitLabel ?? trans('forms.save'), ' icon-paperplane') }}
    </div>
</div>



