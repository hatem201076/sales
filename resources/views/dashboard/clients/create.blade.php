@php($title = trans('clients.actions.add'))

@extends('dashboard.layouts.main', ['title' => $title])

@section('content')

    @component('dashboard.components.page', ['header_title' => $title])

        @slot('breadcrumb')
            <li>
                <a href="{{ dashboard_route('clients.index') }}">
                    @lang('clients.plural')
                </a>
            </li>

            <li class="active">
                {{ $title }}
            </li>
        @endslot

        @component('dashboard.components.panel', [
            'heading' => trans('clients.info'),
            'mode' => 'primary',
        ])

            {{ Form::open(['route' => ['dashboard.clients.store'], 'class' => 'form-horizontal', 'files' => true]) }}

            @include('dashboard.clients.partials.form', ['submitLabel' => trans('forms.add')])

            {{ Form::close() }}

        @endcomponent

    @endcomponent

@endsection
