@php($title = trans('clients.singular') . ' : ' . $client->name)

@extends('dashboard.layouts.main', ['title' => $title])

@section('content')

    @component('dashboard.components.page', ['header_title' => $title])

        @slot('breadcrumb')
            <li>
                <a href="{{ dashboard_route('clients.index') }}">
                    @lang('clients.plural')
                </a>
            </li>

            <li>
                {{ $title }}
            </li>
        @endslot

        @component('dashboard.components.panel', [
            'heading' => $title,
            'mode' => 'primary',
        ])

            {{ Form::model( $client, ['route' => ['dashboard.clients.update', $client], 'method' => 'patch', 'class' => 'form-horizontal', 'files' => true]) }}

            @include('dashboard.clients.partials.form', ['submitLabel' => trans('forms.edit')])

            {{ Form::close() }}

        @endcomponent

    @endcomponent

@endsection
