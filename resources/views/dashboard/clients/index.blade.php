@php($title = trans('clients.plural'))

@extends('dashboard.layouts.main', ['title' => $title])

@section('content')

    @component('dashboard.components.page', ['header_title' => $title])

        @slot('breadcrumb')
            <li class="active">
                {{ $title }}
            </li>
        @endslot

        @can('create', \App\Models\Client::class)
            @slot('header_elements')
                <a href="{{ dashboard_route('clients.create') }}" class="btn btn-primary">
                    @lang('clients.actions.add')
                    <i class="icon-plus3"></i>
                </a>
            @endslot
        @endcan
        @component('dashboard.components.list', ['model' => $clients])
            @slot('thead')
                <th style="font-size: 16px;font-weight: bold;">@lang('clients.attributes.name')</th>
                <th style="font-size: 16px;font-weight: bold;">@lang('clients.attributes.phone')</th>
                <th style="font-size: 16px;font-weight: bold;">...</th>
            @endslot


            @slot('tbody')

                @foreach ($clients as $client)

                    <tr>
                        <td>
                            <a href="{{ dashboard_route('clients.show', $client) }}"  style="font-size: 19px; color: #26a69a;">
                                {{ $client->name }}
                            </a>
                        </td>
                        <td>
                            {{ $client->phone }}

                        </td>

                        <td class=" table-actions">
                            @can('update', $client)
                                <a
                                        href="{{ dashboard_route('clients.edit', $client) }}"
                                        class="btn btn-default text-info btn-xs"
                                >
                                    @lang('forms.edit')
                                    <i class="icon-pencil7"></i>
                                </a>
                            @endcan
                            @can('delete', $client)
                                <a
                                        href="javascript:void(0)"
                                        class="delete-confirm btn btn-default text-danger btn-xs"
                                        data-url="{{ dashboard_route('clients.destroy', $client) }}"
                                        data-title="@lang('clients.dialogs.delete.title', ['item' => $client->name])"
                                        data-message="@lang('clients.dialogs.delete.info')"
                                >
                                    @lang('forms.delete')
                                    <i class="icon-trash"></i>
                                </a>
                            @endcan
                        </td>
                    </tr>
                @endforeach

            @endslot

        @endcomponent

    @endcomponent

@endsection
