@php($title = trans('products.singular') . ' : ' . $product->name)

@extends('dashboard.layouts.main', ['title' => $title])

@section('content')

    @component('dashboard.components.page', ['header_title' => $title])

        @slot('breadcrumb')
            <li>
                <a href="{{ dashboard_route('products.index') }}">
                    @lang('products.plural')
                </a>
            </li>

            <li class="active">
                {{ $title }}
            </li>
        @endslot
        <div class="col-md-12">
            <div class="panel border-left-lg border-left-success">
                <div class="panel-body" style="height:230px">

                    <div class="col-xs-12">
                        <div class="panel-heading"
                             style="background-color: #edf9f8;    padding-bottom: 2px;padding-top: 2px;border-radius: 8px;margin-bottom: 4px;">
                            <h5>
                                <a href="{{ dashboard_route('products.show', $product) }}">{{ $product->name }}</a>
                            </h5>
                        </div>
                        <p class="mb-15" style="font-size: 17px">
                                 <i class=" icon-price-tag" style="color: #263238"></i> @lang('products.attributes.sector')
                              : {{  $product->sector }}
                        </p>
                        <p class="mb-15" style="font-size: 17px">
                                 <i class="  icon-list-numbered"
                                   style="color: {{ $product->count > 0 ? "#7EFC7E" : 'red'}}"></i> @lang('products.attributes.count')
                             : {{  $product->count }}
                        </p>

                        <p class="mb-15"><i class=" icon-magazine"
                                            style="color: #e8aee3;font-size: 23px"></i> {{  $product->description  }}
                        </p>
                        <div style="min-height:8px;max-height: 10px " class="col-sm-12">
                            <a href={{ dashboard_route('products.show', $product) }} title='{{ $product->name }}'></a>
                        </div>
                    </div>
                </div>

                <div class="panel-footer panel-footer-condensed" style="max-height:37px;">

                    <div class="heading-elements">
                <span class="heading-text" title="{{ $product->name }}">
                    <i class="icon-pin" style="color: mediumvioletred"></i>
                    <span class="text-semibold"> {{ $product->store['name'] }}</span>
                </span>

                        <ul class="list-inline list-inline-condensed heading-text pull-right">

                            <li class="dropdown">
                                <a href="#" class="text-default dropdown-toggle" data-toggle="dropdown">
                                    <i class="icon-menu7"></i> <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">

                                    <li><a href="{{ dashboard_route('products.saleForm', $product) }}">
                                            <i class="icon-cart5"></i> بيع الصنف</a>
                                    </li>
                                    <li><a href="{{ dashboard_route('products.installmentForm', $product) }}">
                                            <i class="icon-cart5"></i> تقسيط الصنف</a>
                                    </li>

                                    <li class="divider"></li>
                                @can ('update', $product) <!--  delete and update only for author -->
                                    <li>
                                        <a href="{{ dashboard_route('products.edit', $product) }}" class="btn btn-xs">
                                            @lang('forms.edit')
                                            <i class="icon-pencil7"></i>
                                        </a>
                                    </li>
                                @endcan
                                @can ('delete', $product) <!--  delete and update only for author -->
                                    <li>
                                        <a
                                                href="javascript:void(0)"
                                                class="delete-confirm btn btn-xs"
                                                data-url="{{ dashboard_route('products.destroy', $product) }}"
                                                data-title="@lang('products.dialogs.delete.title', ['item' => $product->name])"
                                                data-message="@lang('products.dialogs.delete.info')"
                                        >
                                            @lang('forms.delete')
                                            <i class="icon-trash"></i>
                                        </a>
                                    </li>
                                    @endcan

                                    @can('detach', $product)
                                        <li>
                                            <a
                                                    href="javascript:void(0)"
                                                    class="delete-confirm btn btn-xs"
                                                    data-url="{{ route('dashboard.detach', $product) }}"
                                                    data-title="@lang('products.dialogs.detach.title', ['item' => $product->name])"
                                                    data-message="@lang('products.dialogs.detach.info')"
                                            >
                                                @lang('forms.detach')
                                                <i class="icon-cross"></i>
                                            </a>
                                        </li>
                                    @endcan

                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    @endcomponent



@endsection