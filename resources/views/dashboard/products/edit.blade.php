@php($title = trans('products.singular') . ' - ' . $product->name)

@extends('dashboard.layouts.main', ['title' => $title])

@section('content')

    @component('dashboard.components.page', ['header_title' => $title])

        @slot('breadcrumb')
            <li>
                <a href="{{ dashboard_route('products.index') }}">
                    @lang('products.plural')
                </a>
            </li>

            <li class="active">
                {{ $title }}
            </li>
        @endslot

        @component('dashboard.components.panel', [
            'heading' => trans('products.info'),
            'mode' => 'primary',
        ])

            {{ Form::model($product, ['route' => ['dashboard.products.update', $product], 'method' => 'patch', 'class' => 'form-horizontal', 'files' => true]) }}

            @include('dashboard.products.partials.form', ['submitLabel' => trans('forms.edit')])

            {{ Form::close() }}

        @endcomponent

    @endcomponent

@endsection
