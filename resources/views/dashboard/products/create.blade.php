@php($title = trans('products.actions.add'))

@extends('dashboard.layouts.main', ['title' => $title])

@section('content')

    @component('dashboard.components.page', ['header_title' => $title])

        @slot('breadcrumb')
            <li>
                <a href="{{ dashboard_route('products.index') }}">
                    @lang('products.plural')
                </a>
            </li>

            <li class="active">
                {{ $title }}
            </li>
        @endslot

        @component('dashboard.components.panel', [
            'heading' => trans('products.info'),
            'mode' => 'primary',
        ])

            {{ Form::open(['route' => ['dashboard.products.store'], 'class' => 'form-horizontal', 'files' => true]) }}

            @include('dashboard.products.partials.form', ['submitLabel' => trans('forms.add')])

            {{ Form::close() }}

        @endcomponent

    @endcomponent

@endsection
