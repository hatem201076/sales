{{ Form::bsNumber(trans('products.attributes.saleCount'), 'sale', null, ['autofocus', 'required', $readonly ?? '']) }}
{{ Form::bsNumber(trans('products.attributes.sale'), 'salecount', null, ['required', $readonly ?? '']) }}
<input type="hidden" name="product_id" value="{{$product->id}}">

<div class="form-group row">
    <div class="col-lg-12">
        {{ Form::bsSubmit($submitLabel ?? trans('forms.save'), ' icon-paperplane') }}
    </div>
</div>



