{{ Form::bsText(trans('products.attributes.name'), 'name', null, ['autofocus', 'required', $readonly ?? '']) }}
{{ Form::bsNumber(trans('products.attributes.count'), 'count', null, ['required', $readonly ?? '']) }}
{{ Form::bsNumber(trans('products.attributes.wholesale'), 'wholesale', null, ['required', $readonly ?? '']) }}
{{ Form::bsNumber(trans('products.attributes.sector'), 'sector', null, ['required', $readonly ?? '']) }}
{{ Form::bsSelect2(trans('products.attributes.trader_id'), 'trader_id' , $traders, null, [ $readonly ?? '']) }}
{{ Form::bsSelect2(trans('stores.singular'), 'store_id', $stores, null, [ $readonly ?? '']) }}
{{ Form::bsTextarea(trans('products.attributes.description'), 'description', null, ['', $readonly ?? '']) }}


<div class="form-group row">
    <div class="col-lg-12">
        {{ Form::bsSubmit($submitLabel ?? trans('forms.save'), ' icon-paperplane') }}
    </div>
</div>



