@php($title = trans('products.plural'))

@extends('dashboard.layouts.main', ['title' => $title])

@section('content')

    @component('dashboard.components.page', ['header_title' => $title])

        @slot('breadcrumb')
            <li class="active">
                {{ $title }}
            </li>
            <span class="text-success" style="padding: 20px;font-size: 20px;">
                @lang('products.attributes.sectors') : {{ $sectors }}
            </span>
            <span class="text-primary" style="padding: 20px;font-size: 20px;">
                @lang('products.attributes.wholesales') : {{ $wholesales }}
            </span>
        @endslot

        @can('create', \App\Models\Product::class)
            @slot('header_elements')
                <a href="{{ dashboard_route('products.create') }}" class="btn btn-primary">
                    @lang('products.actions.add')
                    <i class="icon-plus3"></i>
                </a>
            @endslot
        @endcan
        @component('dashboard.components.list', ['model' => $products])
            @slot('thead')
                <th style="font-size: 16px;font-weight: bold;">@lang('products.attributes.name')</th>
                <th style="font-size: 16px;font-weight: bold;">@lang('products.attributes.store_id')</th>
                <th style="font-size: 16px;font-weight: bold;">@lang('products.attributes.trader_id')</th>
                <th style="font-size: 16px;font-weight: bold;">@lang('products.attributes.count')</th>
                <th style="font-size: 16px;font-weight: bold;">@lang('products.attributes.sector')</th>
                <th style="font-size: 16px;font-weight: bold;">@lang('products.attributes.wholesale')</th>
                <th style="font-size: 16px;font-weight: bold;">...</th>
            @endslot


            @slot('tbody')

                @foreach ($products as $product)

                    <tr>
                        <td>
                            <a href="{{ dashboard_route('products.show', $product) }}"
                               style="font-size: 19px; color: #26a69a"
                               class="text-primary ">
                                {{ $product->name }}
                            </a>
                        </td>

                        <td>
                            @if($product->trader_id != null)

                                <a
                                        href="{{ dashboard_route('stores.show', $product->store) }}"
                                        class="btn btn-default btn-xs"
                                >
                                    {{ $product->store['name'] }}
                                </a>
                            @endif

                        </td>

                        <td>
                            @if($product->trader_id != null)
                                <a
                                        href="{{ dashboard_route('traders.show', $product->trader) }}"
                                        class="btn btn-default btn-xs"
                                >
                                    {{ $product->trader['name'] }}
                                </a>
                            @endif

                        </td>
                        <td>
                            <p style=" color: {{$product->count > 0? 'green' : "red"}} ">{{ $product->count }}</p>
                        </td>
                        <td>
                            <p>{{ $product->sector}}</p>

                        </td>
                        <td>
                            <p>{{ $product->wholesale}}</p>

                        </td>
                        <td>
                            @can('update', $product)
                                <a
                                        href="{{ dashboard_route('products.edit', $product) }}"
                                        class="btn btn-default text-info btn-xs"
                                >
                                    @lang('forms.edit')
                                    <i class="icon-pencil7"></i>
                                </a>
                            @endcan
                            @can('delete', $product)
                                <a
                                        href="javascript:void(0)"
                                        class="delete-confirm btn btn-default text-danger btn-xs"
                                        data-url="{{ dashboard_route('products.destroy', $product) }}"
                                        data-title="@lang('products.dialogs.delete.title', ['item' => $product->name])"
                                        data-message="@lang('products.dialogs.delete.info')"
                                >
                                    @lang('forms.delete')
                                    <i class="icon-trash"></i>
                                </a>
                            @endcan
                        </td>
                    </tr>
                @endforeach

            @endslot


        @endcomponent

        {{--<div class="text-center"> {{ $products->links() }}</div>--}}

    @endcomponent

@endsection
