@php($title = trans('users.singular').' - '.$user->name)

@extends('dashboard.layouts.main', ['title' => $title])

@section('content')

    @component('dashboard.components.page', ['header_title' => $title])

        @slot('breadcrumb')
            <li>
                <a href="{{ dashboard_route('users.index') }}">
                    @lang('users.plural')
                </a>
            </li>

            <li>
                {{ $title }}
            </li>
        @endslot

        @component('dashboard.components.panel', [
            'heading' => $title,
            'mode' => 'primary',
        ])

            {{ Form::model($user, ['route' => ['dashboard.users.update', $user], 'method' => 'patch', 'class' => 'form-horizontal', 'files' => true]) }}

            {{ Form::bsText(trans('users.attributes.name'), 'name', $user->name, ['autofocus', 'required', 'readonly']) }}

            {{ Form::bsText(trans('users.attributes.email'), 'email', $user->email, ['required', 'readonly']) }}

            {{ Form::bsImageDisplay(trans('users.attributes.avatar'), null, $user->getFirstOrDefaultMediaUrl('default', 'thumb')) }}

            {{ Form::close() }}


                @endcomponent

                @endcomponent

@endsection
