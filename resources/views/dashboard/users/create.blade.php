@php($title = trans('users.actions.add'))

@extends('dashboard.layouts.main', ['title' => $title])

@section('content')

    @component('dashboard.components.page', ['header_title' => $title])

        @slot('breadcrumb')
            <li>
                <a href="{{ dashboard_route('users.index') }}">
                    @lang('users.plural')
                </a>
            </li>

            <li class="active">
                {{ $title }}
            </li>
        @endslot

        @component('dashboard.components.panel', [
            'heading' => trans('users.info'),
            'mode' => 'primary',
        ])

            {{ Form::open(['route' => ['dashboard.users.store'], 'method' => 'post', 'class' => 'form-horizontal', 'files' => true]) }}

            @include('dashboard.users.partials.form', ['submitLabel' => trans('forms.add')])

            {{ Form::close() }}

        @endcomponent

    @endcomponent

@endsection
