@php($title = trans('users.singular') . ' - ' . $user->name)

@extends('dashboard.layouts.main', ['title' => $title])

@section('content')

    @component('dashboard.components.page', ['header_title' => $title])

        @slot('breadcrumb')
            <li>
                <a href="{{ dashboard_route('users.index') }}">
                    @lang('users.plural')
                </a>
            </li>

            <li>
                {{ $title }}
            </li>
        @endslot

        @component('dashboard.components.panel', [
            'heading' => $title,
            'mode' => 'primary',
        ])

            {{ Form::model($user, ['route' => ['dashboard.users.update', $user], 'method' => 'patch', 'class' => 'form-horizontal', 'files' => true]) }}

            @include('dashboard.users.partials.form', ['submitLabel' => trans('forms.edit')])

            {{ Form::close() }}

        @endcomponent

    @endcomponent

@endsection
