@php($title = trans('users.plural'))

@extends('dashboard.layouts.main', ['title' => $title])

@section('content')

    @component('dashboard.components.page', ['header_title' => $title])

        @slot('breadcrumb')
            <li class="active">
                {{ $title }}
            </li>
        @endslot

        @can('create', auth()->user())

            @slot('header_elements')
                <a href="{{ dashboard_route('users.create') }}" class="btn btn-primary">
                    @lang('users.actions.add')
                    <i class="icon-plus3"></i>
                </a>
            @endslot

        @endcan

        @component('dashboard.components.list', ['model' => $users])
            @slot('thead')
                <th style="font-size: 16px;font-weight: bold;">@lang('users.attributes.name')</th>
                <th style="font-size: 16px;font-weight: bold;">@lang('users.attributes.email')</th>
                <th style="font-size: 16px;font-weight: bold;">@lang('users.attributes.type')</th>
                {{--<th>@lang('users')</th>--}}
                <th>...</th>
            @endslot

            @slot('tbody')

                @foreach($users as $user)

                    <tr>
                        <td>
                            <a href="{{ dashboard_route('users.show', $user) }}">
                                {{ $user->name }}
                            </a>
                        </td>
                        <td>{{ $user->email }}</td>
                        <td>{{ trans('users.type.'.$user->type) }}</td>
                        <td class="table-actions">

                                @can('update', $user)
                                    <a
                                            href="{{ dashboard_route('users.edit', $user) }}"
                                            class="btn btn-default text-info btn-xs">
                                        @lang('forms.edit')
                                        <i class="icon-pencil7"></i>
                                    </a>
                                @endcan
                            @if( auth()->user()->id != $user->id)
                                @if($user->type != 2)
                                @can('delete', auth()->user())
                                    <a
                                            href="javascript:void(0)"
                                            class="delete-confirm btn btn-default text-danger btn-xs"
                                            data-url="{{ dashboard_route('users.destroy', $user) }}"
                                            data-title="@lang('users.dialogs.delete.title', ['item' => $user->name])"
                                            data-message="@lang('users.dialogs.delete.info')"
                                    >
                                        @lang('forms.delete')
                                        <i class="icon-trash"></i>
                                    </a>
                                @endcan
                                    @endif
                                @endif
                        </td>
                    </tr>
                @endforeach

            @endslot

        @endcomponent

    @endcomponent

@endsection
