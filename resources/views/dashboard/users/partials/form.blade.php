{{ Form::bsText(trans('users.attributes.name'), 'name', null, ['autofocus', 'required', $readonly ?? '']) }}

{{ Form::bsText(trans('users.attributes.email'), 'email', null, ['required', $readonly ?? '']) }}


@if (empty($readonly))

    {{ Form::bsPassword(trans('users.attributes.password'), 'password') }}

    {{ Form::bsPassword(trans('users.attributes.password_confirmation'), 'password_confirmation') }}

    @if(auth()->user()->id  != $user->id )
    {{ Form::bsSelect2(trans('users.attributes.type'), 'type', [0 => trans('users.actions.add'), 1 => trans('managers.actions.add')],
     0, ['required', $readonly ?? '']) }}
    @endif
    {{ Form::bsImage(trans('users.attributes.avatar'), 'avatar', $user->getFirstOrDefaultMediaUrl('default', 'thumb')) }}

    {{ Form::bsSubmit($submitLabel ?? trans('forms.save'), ' icon-paperplane') }}
@else

    {{ Form::bsImageDisplay(trans('users.attributes.avatar'), null, $user->getFirstOrDefaultMediaUrl('default', 'thumb')) }}

@endif

<script src='\js\jquery.min.js' crossorigin="anonymous"></script>
<script src='\js\dependant_divs.js' crossorigin="anonymous"></script>

