@php($title = trans('calculations.plural'))

@extends('dashboard.layouts.main', ['title' => $title])

@section('content')

    @component('dashboard.components.page', ['header_title' => $title])

        @slot('breadcrumb')
            <li class="active">
                {{ $title }}
            </li>
        @endslot
        <div class="col-sm-offset-3 col-sm-6">
            <div>
                {{ Form::open(['route' => ['dashboard.calculations.counts'], 'class' => 'form-horizontal']) }}

                @include('dashboard.sales.partials.formCount', ['submitLabel' => trans('forms.count')])

                {{ Form::close() }}
            </div>


        </div>

    @endcomponent

@endsection