@php($title = trans('calculations.plural'))

@extends('dashboard.layouts.main', ['title' => $title])

@section('content')

    @component('dashboard.components.page', ['header_title' => $title])

        @slot('breadcrumb')
            <li class="active">
                {{ $title }}
            </li>
        @endslot
        <div class="col-sm-offset-3 col-sm-6">
            <div>
                {{ Form::open(['route' => ['dashboard.calculations.counts'], 'class' => 'form-horizontal']) }}

                @include('dashboard.sales.partials.formCount', ['submitLabel' => trans('forms.count')])

                {{ Form::close() }}
            </div>

            <div class="col-sm-offset-3 col-sm-6" style="background-color: white; font-size: 30px">

                     <div class="text-primary">
                        <h4>الأقساط</h4>
                        {{ $installments ? $installments : 'لا يوجد'  }}
                    </div>

                    <hr style="font-size: 10px">

                     <div class="text-success">
                        <h4> المحصل من الأقساط</h4>
                        {{ $collectedInstallments ? $collectedInstallments : 'لا يوجد ' }}
                    </div>
                    <hr style="font-size: 10px">

                     <div class="text-info">
                        <h4> المبيعات </h4>
                        {{ $sales ? $sales : 'لا يوجد'  }}
                    </div>
                    <hr style="font-size: 10px">

                      <div class="text-purple">
                        <h4> المصروفات </h4>
                        {{ $expenses ? $expenses : 'لا يوجد'  }}
                    </div>
                    <hr style="font-size: 10px">
             </div>
        </div>
    @endcomponent

@endsection