{{ Form::bsNumber(trans('products.attributes.saleCount'), 'count', $sale->sale_count? $sale->sale_count: null, ['autofocus', 'required', $readonly ?? '']) }}
{{ Form::bsNumber(trans('products.attributes.sale'), 'cost', $sale->sale? $sale->sale: null, ['required', $readonly ?? '']) }}
{{ Form::bsSelect(trans('installments.attributes.bill'), 'bill_id', $bills, null, [ $readonly ?? '']) }}
<input type="hidden" name="product_id" value="{{ $product->id }}">

<div class="form-group row">
    <div class="col-lg-12">
        {{ Form::bsSubmit($submitLabel ?? trans('forms.save'), ' icon-paperplane') }}
    </div>
</div>



