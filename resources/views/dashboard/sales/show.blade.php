@php($title = trans('products.singular') . ' : ' . $sale->product->name)


@extends('dashboard.layouts.main', ['title' => $title])

@section('content')

    @component('dashboard.components.page', ['header_title' => $title])

        @slot('breadcrumb')
            <li>
                <a href="{{ dashboard_route('sales.index') }}">
                    @lang('sales.plural')
                </a>
            </li>

            <li class="active">
                {{ $title }}
            </li>
        @endslot
        <div class="col-md-12">
            <div class="panel border-left-lg border-left-success">
                <div class="panel-body" style="height:300px">

                    <div class="col-xs-12">
                        <div class="panel-heading"
                             style="background-color: #edf9f8;    padding-bottom: 2px;padding-top: 2px;border-radius: 8px;margin-bottom: 4px;">
                            <h5>
                                <a href="{{ dashboard_route('products.show', $sale->product->id) }}">{{ $sale->product->name }}</a>
                            </h5>
                        </div>
                        <p class="mb-15" style="font-size: 17px">

                            <i class=" icon-price-tag"
                               style="color: #e8aee3"></i> @lang('sales.attributes.sale')
                            : {{  $sale->cost }}
                        </p>
                        <p class="mb-15" style="font-size: 17px">
                                 <i class="  icon-list-numbered"
                                   style="color:#1d75b3"></i> @lang('sales.attributes.count')
                             : {{  $sale->count }}
                        </p>
                        <p class="mb-15" style="font-size: 17px">
                                 <i class="  icon-list-numbered"
                                   style="color: #4A148C"></i> @lang('sales.attributes.saler')
                             : {{  $sale->user->name }}
                        </p>
                        <p class="mb-15" style="font-size: 17px">
                                 <i class="   icon-watch2"
                                   style="color:#7E57C2"></i> @lang('sales.attributes.date')
                             : {{  $sale->created_at->format('Y-m-d') }}
                        </p>


                        <ul class="list-inline list-inline-condensed heading-text pull-right">

                            <li class="dropdown">
                                <a href="#" class="text-default dropdown-toggle" data-toggle="dropdown">
                                    <i class="icon-menu7"></i> <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">

                                @can ('update', $sale) <!--  delete and update only for author -->
                                    <li>
                                        @can('update', $sale)
                                            @php($product = $sale->product)
                                            <a
                                                    href="{{ dashboard_route('sales.edit', [ $sale, $product]) }}"
                                                    class="btn btn-default text-info btn-xs"
                                            >
                                                @lang('forms.edit')
                                                <i class="icon-pencil7"></i>
                                            </a>
                                        @endcan
                                        @can('delete', $sale)
                                            <a
                                                    href="javascript:void(0)"
                                                    class="delete-confirm btn btn-default text-danger btn-xs"
                                                    data-url="{{ dashboard_route('sales.destroy', $sale) }}"
                                                    data-title="@lang('sales.dialogs.delete.title', ['item' => $sale->product['name']])"
                                                    data-message="@lang('sales.dialogs.delete.info')"
                                            >
                                                @lang('forms.delete')
                                                <i class="icon-trash"></i>
                                            </a>
                                        @endcan
                                    </li>
                                    @endcan

                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    @endcomponent



@endsection