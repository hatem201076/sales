@php($title = trans('sales.plural'))

@extends('dashboard.layouts.main', ['title' => $title])

@section('content')

    @component('dashboard.components.page', ['header_title' => $title])

        @slot('breadcrumb')
            <li class="active">
                {{ $title }}
            </li>
        @endslot


        @component('dashboard.components.list', ['model' => $sales])
            @slot('thead')
                <th style="font-size: 16px;font-weight: bold;">@lang('sales.attributes.name')</th>
                <th style="font-size: 16px;font-weight: bold;">@lang('sales.attributes.saler')</th>
                <th style="font-size: 16px;font-weight: bold;">@lang('sales.attributes.sale_count')</th>
                <th style="font-size: 16px;font-weight: bold;">@lang('sales.attributes.sale')</th>
                <th style="font-size: 16px;font-weight: bold;">@lang('sales.attributes.date')</th>
                <th style="font-size: 16px;font-weight: bold;">...</th>
            @endslot


            @slot('tbody')

                @foreach ($sales as $sale)

                    <tr>
                        <td>
                            <a href="{{ dashboard_route('sales.show', $sale) }}"
                               style="font-size: 19px; color: #26a69a"
                               class="text-primary ">
                                @if($sale->product['name'])
                                    {{  $sale->product['name'] }}
                                @else
                                    <h5 class="text-danger"> تم حذف البضاعة</h5>
                                @endif
                            </a>
                        </td>

                        <td>

                            {{ $sale->user->name }}

                        </td>

                        <td>
                            {{ $sale->count }}
                        </td>

                        <td>
                            {{ $sale->cost }}
                        </td>

                        <td>
                            {{ $sale->created_at->format('Y-m-d H:i') }}
                        </td>

                        <td>
                            @can('update', $sale)
                                @php($product = $sale->product)
                                <a
                                        href="{{ dashboard_route('sales.edit', [ $sale, $product]) }}"
                                        class="btn btn-default text-info btn-xs"
                                >
                                    @lang('forms.edit')
                                    <i class="icon-pencil7"></i>
                                </a>
                            @endcan
                            @can('delete', $sale)
                                <a
                                        href="javascript:void(0)"
                                        class="delete-confirm btn btn-default text-danger btn-xs"
                                        data-url="{{ dashboard_route('sales.destroy', $sale) }}"
                                        data-title="@lang('sales.dialogs.delete.title', ['item' => $sale->product['name']])"
                                        data-message="@lang('sales.dialogs.delete.info')"
                                >
                                    @lang('forms.delete')
                                    <i class="icon-trash"></i>
                                </a>
                            @endcan
                        </td>
                    </tr>

                @endforeach

            @endslot


        @endcomponent

        {{--<div class="text-center"> {{ $products->links() }}</div>--}}

    @endcomponent

@endsection
