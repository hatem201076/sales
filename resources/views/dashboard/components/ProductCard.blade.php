<div class="col-md-6">
    <div class="panel border-left-lg border-left-success">
        <div class="panel-body" style="height:200px">

            <div class="col-xs-12 ">
                <div class="panel-heading"
                     style="background-color: #edf9f8;    padding-bottom: 2px;padding-top: 2px;border-radius: 8px;margin-bottom: 4px;">
                    <h5>
                        <a href="{{ dashboard_route('products.show', $product) }}">{{ $product->name }}</a>
                    </h5>
                </div>
                <p class="mb-15" style="font-size: 17px">
                    <span class="text-primary">
                        <i class=" icon-price-tag text-primary"></i> @lang('products.attributes.sector')
                    </span> : {{  $product->sector }}
                </p>
                <p class="mb-15" style="font-size: 17px"><span
                            class=" @if($product->count >0)text-success @else text-danger @endif">
                        <i class="  icon-list-numbered"
                           style="color: {{ $product->count > 0 ? "green" : 'red'}}"></i> @lang('products.attributes.count')
                    </span>
                    : {{  $product->count }}
                </p>

                <div style="min-height:8px;max-height: 10px " class="col-sm-12">
                    <a href={{ dashboard_route('products.show', $product) }} title='{{ $product->name }}'></a>
                </div>
            </div>
        </div>

        <div class="panel-footer panel-footer-condensed" style="max-height:37px;">

            <div class="heading-elements">
                <i class="icon-pin" style="color: mediumvioletred"></i>
                <span class="text-semibold"> {{ $product->store['name'] }}</span>

                <ul class="list-inline list-inline-condensed heading-text pull-right">

                    <li class="dropdown">
                        <a href="#" class="text-default dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-menu7"></i> <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="{{ dashboard_route('products.show', $product ) }}">
                                    <i class=" icon-eye4"></i> مشاهدة الصنف</a>
                            </li>
                            <li><a href="{{ dashboard_route('products.saleForm', $product) }}">
                                    <i class="icon-cart5"></i> بيع الصنف</a>
                            </li>
                            @if(auth()->user()->type != 0 )
                                <li><a href="{{ dashboard_route('products.installmentForm', $product) }}">
                                        <i class="icon-cart5"></i> تقسيط الصنف</a>
                                </li>
                                <li><a href="{{ dashboard_route('installment_groups.create', $product) }}">
                                        <i class="icon-cart5" style="color: #7E57C2"></i>  تقسيط مجموعة</a>
                                </li>
                            @endif

                            <li class="divider"></li>
                            @can ('update', $product) <!--  delete and update only for author -->
                            <li>
                                <a href="{{ dashboard_route('products.edit', $product) }}" class="btn btn-xs">
                                    @lang('forms.edit')
                                    <i class="icon-pencil7"></i>
                                </a>
                            </li>
                            @endcan
                            @can ('delete', $product) <!--  delete and update only for author -->
                            <li>
                                <a
                                        href="javascript:void(0)"
                                        class="delete-confirm btn btn-xs"
                                        data-url="{{ dashboard_route('products.destroy', $product) }}"
                                        data-title="@lang('products.dialogs.delete.title', ['item' => $product->name])"
                                        data-message="@lang('products.dialogs.delete.info')"
                                >
                                    @lang('forms.delete')
                                    <i class="icon-trash"></i>
                                </a>
                            </li>
                            @endcan

                            @can('detach', $product)
                                <li>
                                    <a
                                            href="javascript:void(0)"
                                            class="delete-confirm btn btn-xs"
                                            data-url="{{ route('dashboard.detach', $product) }}"
                                            data-title="@lang('products.dialogs.detach.title', ['item' => $product->name])"
                                            data-message="@lang('products.dialogs.detach.info')"
                                    >
                                        @lang('forms.detach')
                                        <i class="icon-cross"></i>
                                    </a>
                                </li>
                            @endcan

                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>