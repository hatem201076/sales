<div class="form-group{{ $errors->has($name . '[]') ? ' has-error' : '' }}">
    {{ Form::label($name, $label, ['class' => 'control-label col-lg-2']) }}
    <div class="col-lg-10">
        {{ Form::select($name . '[]', $options, $value ?: old($name . '[]'), array_merge(['class' => 'form-control select2', 'id' => $name, 'multiple' => 'multiple'], $attributes ?: [])) }}
        @if ($errors->has($name))
            <strong class="help-block">{{ $errors->first($name) }}</strong>
        @endif
    </div>
</div>
