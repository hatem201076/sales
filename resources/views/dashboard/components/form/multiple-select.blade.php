@php($inputName = $name . '[]')
<div class="form-group row{{ $errors->has($name) ? ' has-error' : '' }}">
    {{ Form::label($name, $label, ['class' => 'control-label col-lg-2']) }}
    <div class="col-lg-10">
        {{ Form::select($inputName, $options, $value ?: old($name), array_merge(['class' => 'form-control selectpicker show-tick', 'id' => $name, 'data-width' => '100%', 'multiple'], $attributes ?: [])) }}

        @if ($errors->has($name))
            <span class="help-block">
                <strong>{{ $errors->first($name) }}</strong>
            </span>
        @endif

    </div>
</div>





