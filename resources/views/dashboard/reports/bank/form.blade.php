
@extends('adminlte::layout.main', ['title' => 'Bank transaction report'])
@section('content')
    @component('adminlte::page', ['title' => 'Bank transaction report', 'breadcrumb' => 'bank_transactions_report'])
        @component('adminlte::box', ['title' => 'Bank transaction report'])

            {{ BsForm::get(route('reports.bank.calculate')) }}

            {{ BsForm::date('date')->label('Date') }}

            {{ BsForm::submit('Submit')->style('inline')->success() }}
            {{ BsForm::submit('Pdf')->style('inline')->name('pdf') }}
            {{ BsForm::submit('Excel')->style('inline')->name('excel') }}

            {{ BsForm::close() }}

        @endcomponent
    @endcomponent
@endsection
