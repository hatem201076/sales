@extends('adminlte::layout.main', ['title' => 'Bank transaction reports'])

@section('content')
    @component('adminlte::page', ['title' => 'Bank transaction reports', 'breadcrumb' => ['report', $parent=3]])
        @component('adminlte::table-box', ['title' => 'Bank transaction report'])
            <canvas id="BankChart"></canvas>
            <tr>
                <th>Deposit</th>
                <td>{{ $bill->insatllment }}</td>
            </tr>
            {{--<tr>--}}

                {{--<th>Withdrawal</th>--}}
                {{--<td>{{ $withdrawal }}</td>--}}
            {{--</tr>--}}
            {{--<tr>--}}

                {{--<th>Income (cheques) </th>--}}
                {{--<td>{{ $income }}</td>--}}
            {{--</tr>--}}
            {{--<tr>--}}

                {{--<th>Outcome (cheques)</th>--}}
                {{--<td>{{ $outcome }}</td>--}}
            {{--</tr>--}}
            {{--<tr>--}}

                {{--<th>Total credit</th>--}}
                {{--<td>{{ $bankBalance }}</td>--}}

            {{--</tr>--}}
        @endcomponent
    @endcomponent
@endsection
@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.bundle.js"></script>
    <script>
        var inBank = "{{ $deposite }}";
        var outBank = "{{ $withdrawal }}";
        var inn = "{{ $income }}";
        var out = "{{ $outcome }}";
        var total = "{{ $bankBalance }}";
        var ctx = document.getElementById("BankChart");
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ['deposit', 'withdrawal', 'income', 'outcome', 'total'],
                datasets: [{
                    label: 'Money',
                    data: [inBank, outBank, inn, out, total],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });
    </script>
@endpush
