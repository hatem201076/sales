@extends('adminlte::layout.main', ['title' => 'Transactions reports'])

@section('content')
    @component('adminlte::page', ['title' => 'Transactions report', 'breadcrumb' => [ 'report', $parent=1]])
        @component('adminlte::table-box', ['title' => 'Category paid amount'])
            <canvas id="CatChart"></canvas>
            <thead>
            <tr>
                <th>Category</th>
                <th>Paid amount</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($data as $value)
                <tr>
                    <td>{{ $value["name"] }}</td>
                    <td>{{ $value["paid_amount"] }}</td>
                </tr>
            @endforeach
            </tbody>
        @endcomponent
    @endcomponent
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.bundle.js"></script>
    <script>
        var ex = JSON.parse('{!! json_encode($data) !!}');
        var categoriesNames = ex.map(function (category) {
            return category.name;
        });
        var categoriesIds = ex.map(function (category) {
            return category.paid_amount;
        });
        var ctx = document.getElementById("CatChart");
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: categoriesNames,
                datasets: [{
                    label: 'paid amount',
                    data: categoriesIds,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });
    </script>
@endpush

