@extends('adminlte::layout.main', ['title' => 'Transactions report form'])
@section('content')
    @component('adminlte::page', ['title' => 'Transactions report form', 'breadcrumb' => 'category_transactions_report'])
        @component('adminlte::box', ['title' => 'Transactions report form'])
            {{ BsForm::get(route('reports.transactions.calculate')) }}

            {{ BsForm::select('type', [
                \App\Models\Transaction::INCOME => 'Income',
                \App\Models\Transaction::OUTCOME => 'Outcome',
            ])->placeholder('Select type')->label('Type') }}

            {{ BsForm::date('from')->label('From') }}

            {{ BsForm::date('to')->label('To') }}

            <div class="form-group">
                {{-- @if (request()->isMethod('post'))--}}
                {{ BsForm::submit('Submit')->style('inline')->success() }}
                {{ BsForm::submit('Pdf')->style('inline')->name('pdf') }}
                {{ BsForm::submit('Excel')->style('inline')->name('excel') }}
                {{--@endif--}}
            </div>

            {{ BsForm::close() }}
        @endcomponent
    @endcomponent
@endsection
