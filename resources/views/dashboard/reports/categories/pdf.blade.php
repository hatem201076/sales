@extends('reports.layout.pdf')

@section('content')

    <h2 align="center" style="padding-bottom: 20px;">Transactions report</h2>
    <table class="table">
        <thead>
        <tr>
            <th>Category</th>
            <th>Paid amount</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($data as $value)
            <tr>
                <td>{{ $value["name"] }}</td>
                <td>{{ $value["paid_amount"] }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection

