@extends('adminlte::layout.main', ['title' => $category->name . ' transactions reports'])

@section('content')
    @component('adminlte::page', ['title' => $category->name . ' transactions reports', 'breadcrumb' => [ 'report', $parent=2]])
        @component('adminlte::table-box', ['title' => 'Subcategory paid amount'])
            <canvas id="SubcatChart"></canvas>
            <thead>
            <tr>
                <th>Subcategory</th>
                <th>Paid amount</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($data as $value)
                <tr>
                    <td>{{ $value["name"]}}</td>
                    <td>{{ $value["paid_amount"] }}</td>
                </tr>
            @endforeach

            </tbody>
        @endcomponent
    @endcomponent
@endsection
@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.bundle.js"></script>
    <script>
        var data = JSON.parse('{!! json_encode($data) !!}');
        var subcategoriesNames = data.map(function (subcategory) {
            return subcategory.name;
        });
        var subcategoriesIds = data.map(function (subcategory) {
            return subcategory.paid_amount;
        });
        var ctx = document.getElementById("SubcatChart");
        new Chart(ctx, {
            type: 'bar',
            data: {
                labels: subcategoriesNames,
                datasets: [{
                    label: 'paid amount',
                    data: subcategoriesIds,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });
    </script>
@endpush
