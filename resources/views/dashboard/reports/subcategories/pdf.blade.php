@extends('reports.layout.pdf')

@section('content')

    <h2 align="center" style="padding-bottom: 20px;">Category transactions report for {{ $maincategory->name }}</h2>
    <table class="table">
        <thead>
        <tr>
            <th>Subcategory</th>
            <th>Paid amount</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($data as $value)
            <tr>
                <td>{{ $value["name"]}}</td>
                <td>{{ $value["paid_amount"] }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection

