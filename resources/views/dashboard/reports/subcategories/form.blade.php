@extends('adminlte::layout.main', ['title' => 'Category transactions report'])

@section('content')
    @component('adminlte::page', ['title' => 'Category transactions report', 'breadcrumb' => 'subcategory_transactions_report'])
        @component('adminlte::box', ['title' => 'Category transactions report'])
            {{ BsForm::get(route('reports.category_transactions.calculate')) }}

            {{ BsForm::select('type', [
                \App\Models\Transaction::INCOME => 'Income',
                \App\Models\Transaction::OUTCOME => 'Outcome',
            ])->placeholder('Select type')->label('Type') }}

            {{ BsForm::date('from')->label('From') }}

            {{ BsForm::date('to')->label('To') }}

            {{ BsForm::select('category', $categories)->placeholder('Select Category')->label('Category') }}

            {{ BsForm::submit('Submit')->style('inline')->success() }}
            {{ BsForm::submit('Pdf')->style('inline')->name('pdf') }}
            {{ BsForm::submit('Excel')->style('inline')->name('excel') }}

            {{ BsForm::close() }}
        @endcomponent
    @endcomponent
@endsection


