@extends('reports.layout.pdf')

@section('content')

    <h2 align="center" style="padding-bottom: 20px;">Payment method report</h2>
    <table class="table">
        <thead>
        <tr>
            <th>Payment Method</th>
            <th>Paid amount</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Cash</td>
            <td>{{ $data["cash"] }}</td>
        </tr>

        <tr>
            <td>Cheque</td>
            <td>{{ $data["cheque"] }}</td>
        </tr>

        <tr>
            <td>Pos</td>
            <td>{{ $data["pos"] }}</td>
        </tr>

        </tbody>
    </table>

@endsection

