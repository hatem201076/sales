@extends('adminlte::layout.main', ['title' => 'Payment reports'])

@section('content')
    @component('adminlte::page', ['title' => 'Payment method report', 'breadcrumb' => [ 'report', $parent=4]])

        @component('adminlte::table-box', ['title' => 'Payment paid amount'])
            <canvas id="PayChart"  ></canvas>

            <thead>
            <tr>
                <th>Payment method</th>
                <th>Paid amount</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Cash</td>
                <td>{{ $data["cash"] }}</td>
            </tr>

            <tr>
                <td>Cheque</td>
                <td>{{ $data["cheque"] }}</td>
            </tr>

            <tr>
                <td>Pos</td>
                <td>{{ $data["pos"] }}</td>
            </tr>

            </tbody>
        @endcomponent
    @endcomponent
@endsection


@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.bundle.js"></script>
    <script>
        var pay = JSON.parse('{!! json_encode($data) !!}');
        var ctx = document.getElementById("PayChart");
        new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ['cash', 'cheque', 'pos'],
                datasets: [{
                    label: 'paid amount',
                    data: [pay['cash'], pay['cheque'], pay['pos']],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });
    </script>
@endpush

