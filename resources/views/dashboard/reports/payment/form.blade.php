@extends('adminlte::layout.main', ['title' => 'Payment Method report'])
@section('content')
    @component('adminlte::page', ['title' => 'Payment method report', 'breadcrumb' => 'payment_type_report'])
        @component('adminlte::box', ['title' => 'Payment mathod report'])
            {{ BsForm::get(route('reports.payment.calculate')) }}

            {{ BsForm::select('type', [
                \App\Models\Transaction::INCOME => 'Income',
                \App\Models\Transaction::OUTCOME => 'Outcome',
            ])->placeholder('Select type')->label('Type') }}

            {{ BsForm::date('from')->label('From') }}

            {{ BsForm::date('to')->label('To') }}

            {{ BsForm::submit('Submit')->style('inline')->success() }}
            {{ BsForm::submit('Pdf')->style('inline')->name('pdf') }}
            {{ BsForm::submit('Excel')->style('inline')->name('excel') }}

            {{ BsForm::close() }}
        @endcomponent
    @endcomponent
@endsection
