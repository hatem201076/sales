<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <style>
        table, th, td {
            border: 1px solid black;
        }

        td, th {
            height: 1.5cm
        }

        body {
            direction: rtl;
            text-alignment: right;
            page-break-after: always;
            font-family: 'chinese font name';

        }
    </style>
</head>
<body>

<div class="container" style="width: 750px">

    @yield('content')

</div>

</body>
</html>