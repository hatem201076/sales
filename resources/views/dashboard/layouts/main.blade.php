@php($siteName = Setting::get('title'))
<!doctype html>
<html lang="{{ config('app.locale') }}" dir="rtl">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ isset($title) ? ($title . ' | ' .  ($siteName)) : $siteName  }}</title>
        <link href="{{ asset('css/css.css') }}" rel="stylesheet">

        @include('dashboard.layouts.partials.styles')
    </head>
    <body>
            <!-- Main navbar -->
            @include('dashboard.layouts.partials.navbar')
            <!-- /main navbar -->

            <!-- Page container -->
            <div id="app" class="page-container">

                <!-- Page content -->
                <div class="page-content">

                    <!-- Main sidebar -->
                    @include('dashboard.layouts.sidebar.main')
                    <!-- /main-sidebar -->

                    {{-- Contains Page Header and Content --}}
                    @yield('content')

                </div>
                <!-- /page-content -->
            </div>
            <!-- /page-container -->

        <!-- Load prepended scripts -->
        @stack('prescripts')

        <!-- Limitless Javascript Files! -->

        <script type="text/javascript" src="{{ url('') }}/assets/dashboard/js/app.js"></script>

        <script src="{{ url('')}}/assets/dashboard/ckeditor/ckeditor.js"></script>


        {{-- Load custom js files. --}}
        @stack('scripts')

    </body>

</html>
