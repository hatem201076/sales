<!-- Global stylesheets -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
<link href="{{ url('') }}/assets/dashboard/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
<link href="{{ url('') }}/assets/dashboard/css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="{{ url('') }}/assets/dashboard/css/core.css" rel="stylesheet" type="text/css">
<link href="{{ url('') }}/assets/dashboard/css/components.css" rel="stylesheet" type="text/css">
<link href="{{ url('') }}/assets/dashboard/css/colors.css" rel="stylesheet" type="text/css">
<link href="{{ url('') }}/assets/dashboard/css/app.css" rel="stylesheet" type="text/css">

<!-- /global stylesheets -->
@stack('styles')
