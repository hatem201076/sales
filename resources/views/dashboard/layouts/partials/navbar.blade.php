<div class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ route('dashboard.home') }}" target="_blank">
            @php($siteName = Setting::get('title', 'البضاعة'))
             {{ $siteName }}
        </a>

        <ul class="nav navbar-nav visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a>
            </li>
        </ul>

        <ul class="nav navbar-nav navbar-right">


            <!-- Notification -->
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <i class="icon-bell3"></i>
                    <span class="visible-xs-inline-block position-right">{{ trans('installments.plural') }}</span>
                    @if(auth()->user()->unreadNotifications()->count() != 0)
                        <span class="badge bg-warning-400">
                                    {{ auth()->user()->unreadNotifications()->count() }}</span>
                    @endif
                </a>

                <div class="dropdown-menu dropdown-content width-350">
                    <div class="dropdown-content-heading">
                        <strong>{{ trans('installments.plural') }}</strong>
                        <ul class="icons-list">
                            <li><a href="#"><i class="icon-compose"></i></a></li>
                        </ul>
                    </div>

                    <ul class="media-list dropdown-content-body">

                        @foreach(auth()->user()->unreadNotifications()->limit(10)->get() as $notification)
                            <li class="media">

                                <a href="{{ dashboard_route('installments.show', ['id' => $notification->data['id']]) }}">
                                    <div class="media-body">
                                        <div class="media-heading">
                                            <span class="text-semibold">{{ $notification->data['installment'] }} ||</span>
                                            <span class="text-semibold text-success">{{ $notification->data['client'] }}</span>
                                            <span class="media-annotation pull-right">{{ $notification->created_at->diffForHumans() }}</span>
                                        </div>

                                    </div>
                                </a>
                            </li>
                        @endforeach
                    </ul>

                    <div class="dropdown-content-footer">
                        <a href="{{ dashboard_route('installments.notifications') }}" class="text-success" style="font-size: 20px" data-popup="tooltip" title="" data-original-title="All messages">مشاهدة كل الإشعارات</a>
                    </div>
                </div>
            </li>
            <!-- /Notification -->




            <li class="dropdown dropdown-user">
                <a  href="{{ route('logout') }}"  onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-switch2"></i>
                    <span>{{ auth()->user()->name }}</span>
                </a>

                 <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                  </form>
            </li>
        </ul>

        {{-- @includeWhen(auth()->user(), 'layouts.partials.notifications.list') --}}

    </div>
</div>
