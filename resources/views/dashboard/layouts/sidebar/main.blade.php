<div class="sidebar sidebar-main">
    <div class="sidebar-content">

        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">

                    <li class="{{ css_route_active('dashboard.home') }}">
                        <a href="{{ dashboard_route('home') }}">
                            <i class="icon-home4"></i>
                            <span>@lang('dashboard.sidebar.links.home')</span>
                        </a>
                    </li>

                    <!-- Sales navigation -->

                    <li class="{{ css_resource_active('dashboard.sales') }}">
                        <a href="{{ dashboard_route('sales.index') }}">
                            <i class="icon-cart5" style="color: yellowgreen"></i>
                            <span>@lang('sales.plural')</span>
                        </a>
                    </li>


                    <!---- Admin navigation ----->

                @if(auth()->user()->isAdmin() || auth()->user()->isPoss())

                    <!-- Sproducts navigation -->

                        <li class="{{ css_resource_active('dashboard.products') }}">
                            <a href="{{ route('dashboard.products.index') }}">
                                <i class="icon-grid4"></i>
                                <span>@lang('products.plural')</span>
                            </a>
                        </li>

                        <!-- Stores navigation -->

                        <li class="{{ css_resource_active('dashboard.stores') }}">
                            <a href="{{ dashboard_route('stores.index') }}">
                                <i class=" icon-location4" style="color: yellowgreen"></i>
                                <span>@lang('stores.plural')</span>
                            </a>
                        </li>
                        <!-- Traders navigation -->

                        <li class="{{ css_resource_active('dashboard.traders') }}">
                            <a href="{{ dashboard_route('traders.index') }}">
                                <i class="icon-person"></i>
                                <span>@lang('traders.plural')</span>
                            </a>
                        </li>
                        <!-- Clients navigation -->

                        <li class="{{ css_resource_active('dashboard.clients') }}">
                            <a href="{{ dashboard_route('clients.index') }}">
                                <i class="icon-person" style="color: yellowgreen"></i>
                                <span>@lang('clients.plural')</span>
                            </a>
                        </li>
                        <!-- Installments navigation -->

                        <li class="{{ css_resource_active('dashboard.installments') }}">
                            <a href="{{ dashboard_route('installments.index') }}">
                                <i class="icon-cabinet"></i>
                                <span>@lang('installments.plural')</span>
                            </a>
                        </li>
                        <!-- Installments group -->

                        <li class="{{ css_resource_active('dashboard.installmentGroups') }}">
                            <a href="{{ dashboard_route('installmentGroups.index') }}">
                                <i class="icon-cabinet" style="color: yellowgreen"></i>
                                <span>@lang('installmentGroups.plural')</span>
                            </a>
                        </li>

                        <!-- Users navigation -->

                        <li class="{{ css_resource_active('dashboard.users') }}">
                            <a href="{{ dashboard_route('users.index') }}">
                                <i class="icon-users"></i>
                                <span>@lang('users.plural')</span>
                            </a>
                        </li>
                        <!-- Calculations navigation -->


                        <li class="treeview {{ css_resource_active('dashboard.calculations') }}">
                            <a href="{{ route('dashboard.calculations.index') }}">
                                <i class="icon-stats-growth" style="color: yellowgreen"></i> <span>{{  trans('calculations.plural') }}</span>
                                <span class="pull-right-container">
                                   <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                        </li>
                        <!-- Bills navigation -->


                        <li class="treeview {{ css_resource_active('dashboard.bills') }}">
                            <a href="{{ route('dashboard.bills.index') }}">
                                <i class="icon-copy"></i> <span>{{  trans('bills.plural') }}</span>
                                <span class="pull-right-container">
                                   <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                        </li>
                        <!-- Payments navigation -->


                        <li class="treeview {{ css_resource_active('dashboard.payments') }}">
                            <a href="{{ route('dashboard.payments.index') }}">
                                <i class=" icon-coin-pound" style="color: yellowgreen"></i> <span>{{  trans('payments.plural') }}</span>
                                <span class="pull-right-container">
                                   <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                        </li>
                        <!-- Expenses navigation -->


                        <li class="treeview {{ css_resource_active('dashboard.expenses') }}">
                            <a href="{{ route('dashboard.expenses.index') }}">
                                <i class=" icon-cash"></i> <span>{{  trans('expenses.plural') }}</span>
                                <span class="pull-right-container">
                                   <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                        </li>
                        <!-- Settings navigation -->

                        <li class="{{ css_resource_active('dashboard.settings') }}">
                            <a href="{{ route('dashboard.settings.index') }}">
                                <i class="icon-cog"></i>
                                <span>@lang('settings.plural')</span>
                            </a>
                        </li>

                @endif

                <!-- /main -->
                </ul>
            </div>

        </div>
        <!-- /main navigation -->
        {{-- @include('dashboard.sidebar.items') --}}
    </div>
</div>
