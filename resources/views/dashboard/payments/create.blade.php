@php($title = trans('payments.actions.add'))

@extends('dashboard.layouts.main', ['title' => $title])

@section('content')

    @component('dashboard.components.page', ['header_title' => $title])

        @slot('breadcrumb')
            <li>
                <a href="{{ dashboard_route('payments.index') }}">
                    @lang('payments.plural')
                </a>
            </li>

            <li class="active">
                {{ $title }}
            </li>
        @endslot

        @component('dashboard.components.panel', [
            'heading' => trans('payments.info'),
            'mode' => 'primary',
        ])

            {{ Form::open(['route' => ['dashboard.payments.store'], 'class' => 'form-horizontal']) }}

            @include('dashboard.payments.partials.form', ['submitLabel' => trans('forms.add')])

            {{ Form::close() }}

        @endcomponent

    @endcomponent

@endsection
