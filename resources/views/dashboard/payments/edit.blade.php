@php($title = trans('payments.singular') . ' : ' . $payment->name)

@extends('dashboard.layouts.main', ['title' => $title])

@section('content')

    @component('dashboard.components.page', ['header_title' => $title])

        @slot('breadcrumb')
            <li>
                <a href="{{ dashboard_route('payments.index') }}">
                    @lang('payments.plural')
                </a>
            </li>

            <li class="active">
                {{ $title }}
            </li>
        @endslot

        @component('dashboard.components.panel', [
            'heading' => trans('bills.info'),
            'mode' => 'primary',
        ])

            {{ Form::model( $payment, ['route' => ['dashboard.payments.update', $payment], 'method' => 'put', 'class' => 'form-horizontal', 'files' => true]) }}

            @include('dashboard.payments.partials.form', ['submitLabel' => trans('forms.edit')])

            {{ Form::close() }}

        @endcomponent

    @endcomponent

@endsection
