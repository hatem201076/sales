@php($title = trans('payments.plural'))

@extends('dashboard.layouts.main', ['title' => $title])

@section('content')

    @component('dashboard.components.page', ['header_title' => $title])

        @slot('breadcrumb')
            <li class="active">
                {{ $title }}
            </li>
        @endslot

        @can('create', \App\Models\Payment::class)
            @slot('header_elements')
                <a href="{{ dashboard_route('payments.create') }}" class="btn btn-primary">
                    @lang('payments.actions.add')
                    <i class="icon-plus3"></i>
                </a>
            @endslot
        @endcan
        @component('dashboard.components.list', ['model' => $payments ])
            @slot('thead')
                <th style="font-size: 16px;font-weight: bold;">@lang('payments.attributes.trader')</th>
                <th style="font-size: 16px;font-weight: bold;">@lang('payments.attributes.amount')</th>
                <th style="font-size: 16px;font-weight: bold;">...</th>
            @endslot


            @slot('tbody')

                @foreach ($payments as $payment )

                    <tr>
                        <td>
                            @if($payment->trader != null)
                                <a href="{{ dashboard_route('traders.show', $payment->trader) }}"
                                   style="font-size: 19px; color: #26a69a;">
                                    {{$payment->trader->name}}
                                </a>
                            @else
                                <h4> لا يوجد إسم </h4>
                            @endif
                        </td>
                        <td>
                            {{ $payment->amount }}
                        </td>


                        <td class=" table-actions">
                            @can('update', $payment)
                                <a
                                        href="{{ route('dashboard.payments.edit', $payment) }}"
                                        class="btn btn-default text-info btn-xs"
                                >
                                    @lang('forms.edit')
                                    <i class="icon-pencil7"></i>
                                </a>
                            @endcan
                            @can('delete', $payment)
                                <a
                                        href="javascript:void(0)"
                                        class="delete-confirm btn btn-default text-danger btn-xs"
                                        data-url="{{ dashboard_route('payments.destroy', $payment) }}"
                                        data-title="@lang('payments.dialogs.delete.title', ['item' => $payment->name])"
                                        data-message="@lang('payments.dialogs.delete.info')"
                                >
                                    @lang('forms.delete')
                                    <i class="icon-trash"></i>
                                </a>
                            @endcan
                        </td>
                    </tr>
                @endforeach

            @endslot

        @endcomponent

    @endcomponent

@endsection
