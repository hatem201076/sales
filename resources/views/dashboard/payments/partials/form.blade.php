{{ Form::bsNumber(trans('payments.attributes.amount'), 'amount', null, ['required', $readonly ?? '']) }}
{{ Form::bsSelect(trans('products.attributes.trader_id'), 'trader_id' , $traders, null, ['required', $readonly ?? '']) }}
{{ Form::bsDate(trans('payments.attributes.date'), 'date', null, ['required', $readonly ?? '']) }}
<div class="form-group row">
    <div class="col-lg-12">
        {{ Form::bsSubmit($submitLabel ?? trans('forms.save'), ' icon-paperplane') }}
    </div>
</div>



