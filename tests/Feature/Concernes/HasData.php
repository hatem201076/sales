<?php

namespace Tests\Feature\Concernes;

trait HasData
{
    /**
     * Get the given data with csrf token.
     *
     * @param array $attributes
     * @return array
     */
    public function setData($attributes = [])
    {
        return array_merge([
            '_token' => csrf_token(),
        ], $attributes);
    }

    /**
     * Get the data that will check with database.
     *
     * @param array $attributes
     * @return array
     */
    public function setDatabaseData($attributes = [])
    {
        return $attributes;
    }
}
