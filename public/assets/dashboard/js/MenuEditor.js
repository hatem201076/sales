(function(global, $) {
    console.log('script executed');
    String.prototype.replaceAll = function(search, replacement) {
        var target = this;
        return target.replace(new RegExp(search, 'g'), replacement);
    }

    function MenuEditor(editorSelector, valueElementSelector, itemTemplateSelector) {
        this.el = document.querySelector(editorSelector + ' ul#items');
        this.jsonInput = $(valueElementSelector);
        this.editorSelector = editorSelector;
        this.valueElementSelector = valueElementSelector;

        var self = this;
        var sortable = Sortable.create(this.el, {
            filter: '.js-remove',
            onFilter: function (evt) {
                var dragged = sortable.closest(evt.item);
                dragged && dragged.parentNode.removeChild(dragged);
                self.updateJson();
            },
            onUpdate: function (evt) {
                self.updateJson();
            }
        });

        $(editorSelector + ' #addItem').click(function() {
            var text = $(editorSelector + ' .text').val();
            var url = $(editorSelector + ' .url').val();

            if(! text || ! url) return false;

            $(editorSelector + ' ul#items').append($(itemTemplateSelector)
                .html()
                .replaceAll('{{ link_url }}', url)
                .replaceAll('{{ link_text }}', text));

            self.updateJson();
            $(editorSelector + ' .text').val('');
            $(editorSelector + ' .url').val('');
            return false;
        });
    }

    MenuEditor.prototype.updateJson = function() {
        var items = [];
        $(this.editorSelector + ' li').each((index, li) => {
            items.push({
                "text": $(li).data('text'),
                "url": $(li).data('url')
            });
        });
        this.jsonInput.val(JSON.stringify(items));
    }
    global.MenuEditor = MenuEditor;

})(window, jQuery);