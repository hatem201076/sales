module.exports = {
  dashboard: {
    less: 'resources/assets/vendor/limitless/less/_main',
    lessOut: 'public/assets/dashboard/css',

    customSass: 'resources/assets/dashboard/sass',
    customSassOut: 'public/assets/dashboard/css',

    icons: 'resources/assets/vendor/limitless/icons',
    iconsOut: 'public/assets/dashboard/css/icons',

    js: './resources/assets/dashboard/js',
    jsOut: 'public/assets/dashboard/js',

    limitlessJs: 'resources/assets/vendor/limitless/js',

    jsCustom: 'resources/assets/dashboard/js',
    jsCustomOut: 'public/assets/dashboard/js',

    images: 'resources/assets/vendor/limitless/images',
    imagesOut: 'public/assets/dashboard/images',
  },
};
