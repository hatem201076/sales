/**
 * This script is responsible for compiling Limitless components.
 */

// import laravel elixir.
const elixir = require('laravel-elixir');

// import elixir components.
require('laravel-elixir-rtl');
require('laravel-elixir-rtl-less');
require('laravel-elixir-rtl-sass');

const { dashboard } = require('./config');

elixir((mix) => {
  /**
   * compile less files (ltr and rtl).
   */
  mix.rtlless('bootstrap.less', dashboard.lessOut, dashboard.less);
  mix.rtlless('colors.less', dashboard.lessOut, dashboard.less);
  mix.rtlless('components.less', dashboard.lessOut, dashboard.less);
  mix.rtlless('core.less', dashboard.lessOut, dashboard.less);
  mix.rtlsass('app.scss', dashboard.customSassOut, dashboard.customSass);

  /**
   * copy icons being used.
   */
  mix.copy(`${dashboard.icons}/icomoon/fonts`, `${dashboard.iconsOut}/icomoon/fonts`);
  mix.styles('styles.css', `${dashboard.iconsOut}/icomoon`, `${dashboard.icons}/icomoon`);

  /**
   * Copy the minified and needed globally javascript files.
   */
  mix.webpack(`${dashboard.js}/app.js`, dashboard.jsOut);
  mix.scripts([
    'plugins/pickers/pickadate/picker.js',
    'plugins/pickers/pickadate/picker.time.js',
    'plugins/pickers/anytime.min.js',
  ], `${dashboard.jsOut}/libs.js`, dashboard.limitlessJs);

  // copy image folder
  mix.copy(dashboard.images, dashboard.imagesOut);
});
