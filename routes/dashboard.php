<?php

/*
|--------------------------------------------------------------------------
| Dashboard Routes
|--------------------------------------------------------------------------
|
| Here is where you can register dashboard routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'HomeController@index')->name('home');

//user crud   /* under testing*/
Route::resource('users', 'UserController');

Route::resource('settings', 'SettingController', ['except' => 'create']);

//products and sales
Route::resource('products', 'ProductController');
//Route::get('products/sale/{product}', 'SaleController@create')->name('products.saleForm');
Route::get('sales', 'SaleController@index')->name('sales.index');
Route::post('sales', 'SaleController@store')->name('sales.store');
Route::get('sales/{sale}', 'SaleController@show')->name('sales.show');
Route::delete('sales/{sale}', 'SaleController@destroy')->name('sales.destroy');
Route::get('sales/products/{product}', 'SaleController@create')->name('products.saleForm');
Route::get('sales/{sale}/products/{product}/edit', 'SaleController@edit')->name('sales.edit');
Route::put('sales/{sale}/products/{product}/edit', 'SaleController@update')->name('sales.update');

//installments
Route::get('installments', 'InstallmentController@index')->name('installments.index');
Route::get('installments/products/{product}', 'InstallmentController@create')->name('products.installmentForm');
Route::post('installments/products', 'InstallmentController@store')->name('installments.store');
Route::post('installments/products/group/{product}', 'InstallmentController@storeGroup')->name('installments.storeGroup');
Route::get('installments/{installment}', 'InstallmentController@show')->name('installments.show');
Route::get('installments/{installment}/products/{product}/edit', 'InstallmentController@edit')->name('installments.edit');
Route::put('installments/{installment}/products/{product}/update', 'InstallmentController@update')->name('installments.update');
Route::delete('installments/{installment}', 'InstallmentController@destroy')->name('installments.destroy');
Route::post('installmentsCount', 'CalculateController@installments')->name('installments.count');
Route::post('installmentsCollectedCount', 'CalculateController@collectedInstallments')->name('installments.collected.count');
Route::get('notifications', 'InstallmentController@notifications')->name('installments.notifications');

Route::resource('installmentGroups', 'InstallmentGroupController');
Route::get('installmentGroups/products/{product}', 'InstallmentController@group')->name('installment_groups.create');

//stores traders clients
Route::resource('stores', 'StoreController');
Route::resource('traders', 'TraderController');
Route::get('traders/{trader}/payments', 'TraderController@payments')->name('traders.payments');
Route::resource('clients', 'ClientController');

//calculations
Route::get('calculations', 'CalculateController@index')->name('calculations.index');
Route::post('calculations', 'CalculateController@counts')->name('calculations.counts');

Route::post('insert', 'SettingController@put')->name('insert');



Route::get('search', 'HomeController@search')->name('search');
Route::get('search/{store}', 'StoreController@search')->name('store.search');

Route::resource('collecteds','CollectedController');
Route::get('collecteds/installments/{installment}/index','CollectedController@index')->name('collecteds.index');
Route::get('collecteds/installments/{installment}','CollectedController@create')->name('collecteds.create');
Route::get('collecteds/{collected}/installments/{installment}','CollectedController@edit')->name('collecteds.edit');
Route::put('collecteds/{collected}/installments/{installment}','CollectedController@update')->name('collecteds.update');

Route::resource('bills', 'BillController');
Route::get('bills/{bill}/pdf', 'BillController@pdf')->name('bills.pdf');
Route::get('excel', 'BillController@excelReport')->name('bills.excel');

Route::resource('payments', 'PaymentController');
Route::resource('expenses', 'ExpenseController');
