<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Product;
use App\Models\Store;
use App\Models\Trader;
use Illuminate\Http\Request;
use App\Http\Requests\Dashboard\StoreRequest;

class StoreController extends Controller
{
    /**
     * StoreController constructor.
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stores = Store::latest()->paginate();

        return view('dashboard.stores.index', compact('stores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $store = new Store;

        return view('dashboard.stores.create', compact('store'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        Store::create($request->all());

        return $this->redirectToIndexWithFlash('created');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Store $store
     * @return \Illuminate\Http\Response
     */
    public function show(Store $store)
    {
        $products = $store->products()->latest()->paginate();
        $products2 = $store->products()->where('count', '>', 0)->get();
        $sectors = $store->products()->sum('all_sector');
        $wholesales = $store->products()->sum('all_wholesale');

        return view('dashboard.stores.show', compact('store', 'products', 'wholesales', 'sectors'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Store $store
     * @return \Illuminate\Http\Response
     */
    public function edit(Store $store)
    {
        return view('dashboard.stores.edit', compact('store'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Store $store
     * @return \Illuminate\Http\Response
     */
    public function update(StoreRequest $request, Store $store)
    {
        $store->update($request->all());
        return $this->redirectToIndexWithFlash('updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Store $store
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store)
    {
        $store->delete();
        return $this->redirectToIndexWithFlash('deleted');
    }


}
