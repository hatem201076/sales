<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Requests\Dashboard\InstallmentRequest;
use App\Models\Bill;
use App\Models\Client;
use App\Models\Installment;
use App\Models\InstallmentGroup;
use App\Models\Product;
use App\Models\Store;
use App\Models\Trader;
use Carbon\Carbon;
use Illuminate\Http\Request;

class InstallmentController extends Controller
{

    /**
     * InstallmentController constructor.
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $installments = Installment::latest()->paginate();

        return view('dashboard.installments.index', compact('installments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Product $product)
    {
        $installment = new Installment;
        $clients = Client::all()->pluck('name', 'id');
        $bills = Bill::where('type', '=', '1')->pluck('name', 'id');
        return view('dashboard.installments.create', compact('bills', 'clients', 'product', 'traders', 'stores', 'installment'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(InstallmentRequest $request, Product $product)
    {
        $installment = new Installment;
        $installment->client_id = $request->client_id;
        $installment->count = $request->count;
        $installment->cost = $request->cost;
        $installment->all = $request->cost * $request->count;
        $installment->start_date = $request->start_date;
        $installment->end_date = $request->end_date;
        $installment->bill_id = $request->bill_id;
        $installment->product_id = $request->product_id;
        $installment->user_id = auth()->user()->id;

        //count days and amount
        $start = Carbon::parse($installment->start_date);
        $end = Carbon::parse($installment->end_date);
        $amount = $installment->installment;
        $interval = \DateInterval::createFromDateString('1 month');
        $period = new \DatePeriod($start, $interval, $end);
        $data = [];
        $days = [];
        foreach ($period as $key => $dt) {
            $days[] = $dt->format('d');
        }

        if ($days == []) {
            $installment->day = null;
        } else {
            $installment->day = $days['0'];
        }

        $installment->save();
        $installment->product->decrement('count', $request->count);

        return $this->redirectToIndexWithFlash('created');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Installment $installment
     * @return \Illuminate\Http\Response
     */
    public function show(Installment $installment)
    {
        //count days and amount
        $start = Carbon::parse($installment->start_date);
        $end = Carbon::parse($installment->end_date);
        $amount = $installment->cost;
        $interval = \DateInterval::createFromDateString('1 month');
        $period = new \DatePeriod($start, $interval, $end);
        $data = [];
        $monthes = [];

        foreach ($period as $key => $dt) {
            $monthes[] = $dt->format('Y-m-d');
        }
        //if user enter wrong date
        if ($monthes == []) {
            $monthes = null;
            $cost = null;
            $count_monthes = null;
            $message = 'تاريخ خطأ';
        } else {
            foreach ($monthes as $key => $month) {
                $count_monthes = count((array)$monthes);
                $cost = round($amount / $count_monthes);
                $data[$key]['date'] = $month;
                $data[$key]['cost'] = $cost;
            }
        }

        $sum = $installment->collecteds->sum('collected');

        return view('dashboard.installments.show', compact('sum', 'installment', 'cost', 'count_monthes', 'period', 'monthes', 'message'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Installment $installment
     * @return \Illuminate\Http\Response
     */
    public function edit(Installment $installment, Product $product)
    {
        $traders = Trader::latest()->pluck('name', 'id');
        $stores = Store::latest()->pluck('name', 'id');
        $clients = Client::all()->pluck('name', 'id');
        $bills = Bill::all()->pluck('name', 'id');

        return view('dashboard.installments.edit', compact('bills', 'clients', 'product', 'traders', 'stores', 'installment'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Installment $installment
     * @return \Illuminate\Http\Response
     */
    public function update(InstallmentRequest $request, Installment $installment, Product $product)
    {
        //return number of product in store to can edit again
        $product->increment('count', $installment->count);

        $installment->client_id = $request->client_id;
        $installment->count = $request->count;
        $installment->cost = $request->cost;
        $installment->all = $request->cost * $request->count;
        $installment->start_date = $request->start_date;
        $installment->end_date = $request->end_date;
        $installment->product_id = $request->product_id;
        $installment->user_id = auth()->user()->id;
        $installment->bill_id = $request->bill_id;

//count days and amount
        $start = Carbon::parse($installment->start_date);
        $end = Carbon::parse($installment->end_date);
        $amount = $installment->cost;
        $interval = \DateInterval::createFromDateString('1 month');
        $period = new \DatePeriod($start, $interval, $end);
        $data = [];
        $days = [];
        foreach ($period as $key => $dt) {
            $days[] = $dt->format('d');
        }

        if ($days == []) {
            $installment->day = null;
        } else {
            $installment->day = $days['0'];
        }

        $installment->update();

        $installment->product->decrement('count', $request->count);

        return $this->redirectToIndexWithFlash('updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Installment $installment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Installment $installment)
    {
        $installment->product->increment('count', $installment->count);

        $installment->delete();

        return $this->redirectToIndexWithFlash('deleted');


    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function notifications()
    {
        $notifictions = auth()->user()->unreadNotifications()->limit(50)->get();
        return view('dashboard.installments.notifications', compact('notifictions'));
    }


    //actions in store group
    public function group(Product $product)
    {
        $installment = new Installment;
        $groups = InstallmentGroup::all()->pluck('name', 'id');
        return view('dashboard.installments.partials.installmentGroupForm', compact('groups', 'product'));

    }

    public function storeGroup(InstallmentRequest $request, Product $product)
    {
        $group = InstallmentGroup::where('id', '=', $request->group_id)->first();
        $installment = new Installment;
        $installment->client_id = $group->client->id;
        $installment->count = $request->count;
        $installment->cost = $request->cost;
        $installment->all = $request->cost * $request->count;
        $installment->start_date = $group->start_date;
        $installment->end_date = $group->end_date;
        $installment->bill_id = $group->bill_id;
        $installment->product_id = $product->id;
        $installment->group_id = $group->id;
        $installment->type = 'group';
        $installment->user_id = auth()->user()->id;

        //count days and amount
        $start = Carbon::parse($installment->start_date);
        $end = Carbon::parse($installment->end_date);
        $amount = $installment->installment;
        $interval = \DateInterval::createFromDateString('1 month');
        $period = new \DatePeriod($start, $interval, $end);
        $data = [];
        $days = [];
        foreach ($period as $key => $dt) {
            $days[] = $dt->format('d');
        }

        if ($days == []) {
            $installment->day = null;
        } else {
            $installment->day = $days['0'];
        }


        $installment->save();
        $installment->product->decrement('count', $request->count);

        return $this->redirectToIndexWithFlash('created');

    }
}
