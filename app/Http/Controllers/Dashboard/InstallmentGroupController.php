<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Bill;
use App\Models\Client;
use App\Models\Installment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\InstallmentGroup;

class InstallmentGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $installments = InstallmentGroup::latest()->paginate();
        return view('dashboard.installmentGroups.index', compact('installments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $installment = new Installment;
        $clients = Client::all()->pluck('name', 'id');
        $bills = Bill::where('type', '=', '1')->pluck('name', 'id');
        return view('dashboard.installmentGroups.create', compact('bills', 'clients', 'installment'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());

        $installment2 = new Installment;
        $installment2->client_id = $request->client_id;
        $installment2->bill_id = $request->bill_id;
        $installment2->start_date = $request->start_date;
        $installment2->end_date = $request->end_date;

        $start = Carbon::parse($installment2->start_date);
        //$start->modify('first day of this month');
        $end = Carbon::parse($installment2->end_date);
        //$end->modify('first day of next month');
        $interval = \DateInterval::createFromDateString('1 month');
        $period = new \DatePeriod($start, $interval, $end);
        $data = [];
        $days = [];
        foreach ($period as $key => $dt) {
            $days[] = $dt->format('d');
        }

        if ($days == []) {
            $installment2->day = null;
        } else {
            $installment2->day = $days['0'];
        }

        $installment2->save();


        $installment = new InstallmentGroup;
        $installment->name = $request->name;
        $installment->client_id = $request->client_id;
        $installment->bill_id = $request->bill_id;
        $installment->start_date = $request->start_date;
        $installment->end_date = $request->end_date;
        $installment->save();

        return redirect()->route('dashboard.installmentGroups.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\InstallmentGroup $installmentGroup
     * @return \Illuminate\Http\Response
     */
    public function show(InstallmentGroup $installmentGroup)
    {
        $installments = $installmentGroup->installments;
        return view('dashboard.installmentGroups.show', compact('installments', 'installmentGroup'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\InstallmentGroup $installmentGroup
     * @return \Illuminate\Http\Response
     */
    public function edit(InstallmentGroup $installmentGroup)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\InstallmentGroup $installmentGroup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InstallmentGroup $installmentGroup)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\InstallmentGroup $installmentGroup
     * @return \Illuminate\Http\Response
     */
    public function destroy(InstallmentGroup $installmentGroup)
    {
        //
    }
}
