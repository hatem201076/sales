<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Expense;
use App\Models\Installment;
use App\Models\Sale;
use Illuminate\Http\Request;

class CalculateController extends Controller
{
    public $sales;
    public $installments;
    public $collectedInstallments;

    /**
     * UserController constructor.
     */
    public function __construct()
    {
        $this->middleware('admin');

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
          return view('dashboard.calculations.index');
    }



    public function counts(Request $request)
    {
        $from = $request->from;
        $to = $request->to;
        $sales = Sale::whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)->sum('all');
        $installments = Installment::whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)->sum('all');
        $collectedInstallments = Installment::whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)->sum('collected');
        $expenses = Expense::whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)->sum('amount');

        return view('dashboard.calculations.show', compact('expenses','collectedInstallments','installments','sales'));

    }
//
//    public function installments(Request $request)
//    {
//        $from = $request->from;
//        $to = $request->to;
//        $installments = Installment::where('created_at', '>=', $from)->where('created_at', '<=', $to)->sum('all');
//        $this->installments = $installments;
//
//    }
//
//    public function collectedInstallments(Request $request)
//    {
//        $from = $request->from;
//        $to = $request->to;
//        $collectedInstallments = Installment::where('created_at', '>=', $from)->where('created_at', '<=', $to)->sum('collected');
//        $this->collectedInstallments = $collectedInstallments;
//
//    }
}
