<?php

namespace App\Http\Controllers\Dashboard;

use Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{


    /**
     * Return a list of site settings.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('dashboard.settings.index');
    }

    /**
     * Save a settings into a site settings.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function put(Request $request)
    {
        $items = $request->except('_token', '_method');

        foreach ($items as $key => $value) {
            Setting::set($key, $value);
        }

        $this->flash('saved', 'success', 'settings');

        return back();
    }
}
