<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Trader;
use Illuminate\Http\Request;
use App\Http\Requests\Dashboard\TraderRequest;

class TraderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $traders = Trader::latest()->paginate();

        return view('dashboard.traders.index', compact('traders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $traders = new Trader;

        return view('dashboard.traders.create', compact('traders'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(TraderRequest $request)
    {
        Trader::create($request->all());

        return $this->redirectToIndexWithFlash('created');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Trader $trader
     * @return \Illuminate\Http\Response
     */
    public function show(Trader $trader)
    {
        $products = $trader->products()->latest()->paginate();
        $products2 = $trader->products()->where('count', '>', 0)->paginate();
        $wholesales = ($products2->sum('wholesale')) * ($products->sum('count'));
        $sectors = ($products2->sum('sector')) * ($products->sum('count'));
        $amount = $trader->payments->sum('amount');
        return view('dashboard.traders.show', compact('trader', 'products', 'sectors', 'wholesales', 'amount'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Trader $trader
     * @return \Illuminate\Http\Response
     */
    public function edit(Trader $trader)
    {
        return view('dashboard.traders.edit', compact('trader'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Trader $trader
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Trader $trader)
    {
        $trader->update($request->all());
        return $this->redirectToIndexWithFlash('updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Trader $trader
     * @return \Illuminate\Http\Response
     */
    public function destroy(Trader $trader)
    {
        $trader->delete();
        return $this->redirectToIndexWithFlash('deleted');

    }

    public function payments(Trader $trader)
    {
        $payments = $trader->payments()->paginate();

        return view('dashboard.traders.payments', compact('payments', 'trader'));

    }
}
