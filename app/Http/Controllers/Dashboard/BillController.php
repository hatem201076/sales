<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Bill;
use PDF;
use Excel;
use Illuminate\Http\Request;
use App\Services\BillService;

class BillController extends Controller
{

    public $bill;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bills = Bill::latest()->paginate(15);

        return view('dashboard.bills.index', compact('bills'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bill = new Bill;
        return view('dashboard.bills.create', compact('bill'));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        Bill::create($request->all());

        return $this->redirectToIndexWithFlash('created');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Bill $bill
     * @return \Illuminate\Http\Response
     */
    public function show(Bill $bill)
    {
        if ($bill->type == 1) {
            $installments = $bill->installments;
        } elseif ($bill->type == 2) {
            $installments = $bill->sales;
        }
        //$installments = $bill->installments ? $bill->installments : $bill->sales;

        return view('dashboard.bills.show', compact('bill', 'installments'));

    }

    /**
     * @param Bill $bill
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Bill $bill)
    {
        if ($bill->type = 1) {
            $installments = $bill->installments;
        }
        if ($bill->type = 2) {
            $installments = $bill->sales;
        }

        return view('dashboard.bills.edit', compact('bill', 'installments'));

    }

    /*
     *
     */
    public function update(Request $request, Bill $bill)
    {
        $bill->update($request->all());

        return $this->redirectToIndexWithFlash('updated');

    }

    public function destroy(Bill $bill)
    {
        $bill->delete();
        return $this->redirectToIndexWithFlash('deleted');

    }


    /**
     * @return pdf show and download
     */
    public function pdf(Bill $bill)
    {
        if ($bill->type == 1) {
            $installments = $bill->installments;
        } else{
            $installments = $bill->sales;
        }

        //
        //$installments = $bill->installments ? $bill->installments : $bill->sales;
        if ($bill->type ==1) {
            $all = $bill->installments->sum('all');
        }elseif($bill->type == 2) {
            $all = $bill->sales->sum('all');
        }
        $pdf = PDF::loadView('dashboard.bills.pdf', compact('all', 'installments', 'sales', 'bill'));
        //return $pdf->download('dashboard.bills.pdf');
        return $pdf->stream('dashboard.bills.pdf');
    }


}