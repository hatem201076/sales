<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Collected;
use App\Models\Installment;
use Illuminate\Http\Request;

class CollectedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Installment $installment)
    {
        $collecteds = $installment->collecteds;

         return view('dashboard.collecteds.index', compact('collecteds','installment'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Installment $installment)
    {
        $collected = new Collected ;
        return view('dashboard.collecteds.create', compact( 'collected','installment'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Installment $installment)
    {
        $collected = new Collected ;
        $collected->date = $request->date;
        $collected->installment_id = $request->installment_id;
        $collected->collected = $request->collected;

        $collected->save();

        $collected->installment->increment('collected', $request->collected);

        flash(trans('collecteds.flash.created'))->success();

        return redirect()->route( 'dashboard.collecteds.index', $collected->installment);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ollected  $collected
     * @return \Illuminate\Http\Response
     */
    public function show(Collected $collected)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Collected  $collected
     * @return \Illuminate\Http\Response
     */
    public function edit(Collected $collected, Installment $installment)
    {
         return view('dashboard.collecteds.edit', compact( 'collected','installment'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Collected  $collected
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Collected $collected)
    {
        $collected->installment->decrement('collected', $collected->collected);

        $collected->date = $request->date;
        $collected->installment_id = $request->installment_id;
        $collected->collected = $request->collected;

        $collected->update();

        $collected->installment->increment('collected', $request->collected);

        flash(trans('collecteds.flash.updated'))->success();

        return redirect()->route( 'dashboard.collecteds.index', $collected->installment);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Collected  $collected
     * @return \Illuminate\Http\Response
     */
    public function destroy(Collected $collected)
    {
        $collected->installment->decrement('collected', $collected->collected);

        $collected->delete();

        flash(trans('collecteds.flash.deleted'))->success();

        return redirect()->route( 'dashboard.collecteds.index', $collected->installment);
    }
}
