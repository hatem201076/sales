<?php

namespace App\Http\Controllers\Dashboard;


use Illuminate\Http\Request;
use App\Models\Installment;
use App\Models\Product;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::where('count','>', 0)->latest()->paginate(10);
        return view('dashboard.home', compact('products'));
    }

    /**
     * search items
     */
    public function search(Request $request)
    {
        $products = Product::where('name', 'like', '%'.$request->text.'%')->where('count', '>', 0)->paginate(10);
        return view('dashboard.search', compact('products'));
    }
}
