<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Bill;
use App\Models\Sale;
use App\Models\Store;
use App\Models\Trader;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Requests\Dashboard\SaleRequest;

class SaleController extends Controller
{
    /**
     * SaleController constructor.
     */
    public function __construct()
    {
        //$this->middleware('admin')->except('index', 'edit', 'update');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sales = Sale::latest()->paginate(10);
        return view('dashboard.sales.index', compact('sales'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Product $product)
    {
        $sale = new Sale;
        $bills = Bill::where('type', '=', '2')->pluck('name', 'id');
        $traders = Trader::latest()->pluck('name', 'id');
        $stores = Store::latest()->pluck('name', 'id');

        return view('dashboard.sales.create', compact('bills', 'product', 'traders', 'stores', 'sale'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(SaleRequest $request, Product $product)
    {
        $user = auth()->user();
        if ($user->type = 2) {
             $sale = new Sale;
            $sale->all = $request->count * $request->cost;
            $sale->user_id = auth()->user()->id;
            $sale->product_id = $request->product_id;
            $sale->bill_id = $request->bill_id;
            $sale->count = $request->count;
            $sale->cost = $request->cost;


            $sale->save();
            $sale->product->decrement('count', $request->count);

            return $this->redirectToIndexWithFlash('Created');

            //user is not admin
        } elseif ($user->type = 0) {
            if ($request->cost < $product->sector) {
                $sale = new Sale;
                $sale->all = $request->count * $request->cost;
                $sale->user_id = auth()->user()->id;
                $sale->product_id = $request->product_id;
                $sale->bill_id = $request->bill_id;
                $sale->count = $request->count;
                $sale->cost = $request->cost;


                $sale->save();
                $sale->product->decrement('count', $request->count);

                return $this->redirectToIndexWithFlash('Created');
            } else {
                redirect()->back()->with('danger', 'السعر أقل من السعر المناسب');
            }
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Sale $sale
     * @return \Illuminate\Http\Response
     */
    public function show(Sale $sale)
    {
        return view('dashboard.sales.show', compact('sale'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Sale $sale
     * @return \Illuminate\Http\Response
     */
    public function edit(Sale $sale)
    {
        $product = $sale->product;
        $bills = Bill::all()->pluck('name', 'id');
        $traders = Trader::latest()->pluck('name', 'id');
        $stores = Store::latest()->pluck('name', 'id');
        return view('dashboard.sales.edit', compact('bills', 'product', 'traders', 'stores', 'sale'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Sale $sale
     * @return \Illuminate\Http\Response
     */
    public function update(SaleRequest $request, Sale $sale, Product $product)
    {
        $product = $sale->product;
        //return number of product in store to can edit again
        $product->increment('count', $sale->count);

        $sale->all = $request->count * $request->cost;
        $sale->user_id = auth()->user()->id;
        $sale->product_id = $request->product_id;
        $sale->bill_id = $request->bill_id;
        $sale->count = $request->count;
        $sale->cost = $request->cost;

        $sale->update();

        $product->decrement('count', $request->count);

        return $this->redirectToIndexWithFlash('Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Sale $sale
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sale $sale, Product $product)
    {
        $product->increment('count', $sale->count);

        $sale->delete();

        return $this->redirectToIndexWithFlash('Deleted');


    }
}
