<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Store;
use App\Models\Trader;
use App\Models\User;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Requests\Dashboard\ProductRequest;

class ProductController extends Controller
{



    public function __construct()
    {
        $this->middleware('admin')->except('show');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::latest()->paginate(10);
        $products2 = Product::latest()->where('count', '>', 0);
        $wholesales = ($products2->sum('wholesale'))*($products->sum('count'));
        $sectors = ($products2->sum('sector'))*($products->sum('count'));
        return view('dashboard.products.index', compact('products', 'wholesales', 'sectors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $product = new Product;
        $traders = Trader::latest()->pluck('name', 'id');
        $stores = Store::latest()->pluck('name', 'id');

        return view('dashboard.products.create', compact('product', 'traders', 'stores'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {

        $product = auth()->user()->products()->create($request->all());
        $product->all_sector = ($request->count)*($request->sector);
        $product->all_wholesale = ($request->count)*($request->wholesale);
        $product->save();
        flash(trans('products.flash.created'))->success();

        return redirect()->route('dashboard.products.show', $product);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('dashboard.products.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $this->authorize('update', $product);
        $traders = Trader::latest()->pluck('name', 'id');
        $stores = Store::latest()->pluck('name', 'id');

        return view('dashboard.products.edit', compact('product', 'traders', 'stores'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, Product $product)
    {
        $this->authorize('update', $product);

        $product->update($request->all());
        flash(trans('products.flash.updated'))->success();

        return redirect()->route('dashboard.products.show', $product);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
        flash(trans('products.flash.deleted'))->success();

        return redirect()->to('/');

    }

}
