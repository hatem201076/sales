<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * The default page size for eloquent.
     *
     * @var int
     */
    protected $pageSize = 15;

    /**
     * Send a flash message and Redirect to index.
     *
     * @param  string $event
     * @param  string $level
     * @param  string $resource
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function redirectToIndexWithFlash($event = 'created', $level = 'success', $resource = null)
    {
        $this->flash($event, $level, $resource);


        return $this->redirectToIndex($resource);
    }

    /**
     * Send a flash message.
     *
     * @param  string $event
     * @param  string $level
     * @param  string $resource
     *
     * @return \Laracasts\Flash\FlashNotifier
     */
    public function flash($event = 'created', $level = 'success', $resource = null)
    {
        if (! $resource) {
            $resource = $this->guessResourceName();
        }

        return flash(trans($resource . '.messages.' . $event), $level);
    }

    /**
     * Redirect to index.
     *
     * @param null $resource
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function redirectToIndex($resource = null)
    {
        if (! $resource) {
            $resource = $this->guessResourceName();
        }

        return redirect()->route("dashboard.$resource.index");
    }

    public function redirectToBack($resource = null)
    {
        if (! $resource) {
            $resource = $this->guessResourceName();
        }

        return redirect()->route("create");
    }

    /**
     * Guess the resource name of the controller.
     *
     * @return string
     */
    protected function guessResourceName()
    {
        $classBaseName = class_basename(get_class($this));

        return str_plural(snake_case(str_replace('Controller', '', $classBaseName)));
    }
}
