<?php

namespace App\Http\Requests\Dashboard;

use Illuminate\Foundation\Http\FormRequest ;

class TraderRequest extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->getAproperRules();
    }

    /**
     * Return the rules of create request.
     *
     * @return array
     */
    public function createRules()
    {
        return [
            'name' => 'required|string|max:200',
            'phone' =>'nullable|numeric|min:0|max:999999999999999',
        ];
    }

    /**
     * Return the rules of update request.
     *
     * @return array
     */
    public function updateRules()
    {
        $ignoredId = $this->route('manager') ? $this->route('manager')->id : null;

        return [
            'name' => 'required|string|max:200',
            'phone' =>'nullable|numeric|min:0|max:999999999999999',

        ];

    }


    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return trans('stores.attributes');
    }


}
