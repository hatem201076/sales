<?php

namespace App\Http\Requests\Dashboard;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->getAproperRules();
    }

    /**
     * Return the rules of create request.
     *
     * @return array
     */
    public function createRules()
    {
        return [
            'name' => 'required|string|max:200',
            'count' => 'required|numeric',
            'wholesale' => 'required|numeric',
            'sector' => 'required|numeric',
            'trader_id' => 'numeric|exists:traders,id',
            'store_id' => 'numeric|exists:stores,id',
            'description' => 'nullable|string',
        ];
    }

    /**
     * Return the rules of update request.
     *
     * @return array
     */
    public function updateRules()
    {
        $ignoredId = $this->route('product') ? $this->route('product')->id : null;

        return [
            'name' => 'sometimes|string|max:200',
            'count' => 'sometimes|numeric',
            'wholesale' => 'sometimes|numeric',
            'sector' => 'sometimes|numeric',
            'trader_id' => 'sometimes|numeric|exists:traders,id',
            'store_id' => 'sometimes|numeric|exists:stores,id',
            'description' => 'sometimes|nullable|string',
        ];

    }

    public function messages()
    {
        return [
            'name.required' => 'قيمة حقل إسم الصنف غير صحيحة!',
            'name.string' => 'قيمة حقل إسم الصنف غير صحيحة!',
            'count.required' => 'قيمة حقل العدد المتاح غير صحيحة!',
            'count.numeric' => 'قيمة حقل العدد المتاح غير صحيحة!',
            'wholesale.required' => 'قيمة حقل سعر الجملة غير صحيحة!',
            'wholesale.numeric' => 'قيمة حقل سعر الجملة غير صحيحة!',
            'sector.required' => 'قيمة حقل أقل سعر للبيع غير صحيحة!',
            'sector.numeric' => 'قيمة حقل أقل سعر للبيع غير صحيحة!',
            'trader_id.exists' => 'قيمة حقل التاجر غير صحيحة!',
            'store_id.exists' => 'قيمة حقل التاجر غير صحيحة!',
        ];
    }


}
