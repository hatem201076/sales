<?php

namespace App\Http\Requests\Dashboard;

use Illuminate\Foundation\Http\FormRequest;

class SaleRequest extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->getAproperRules();
    }

    /**
     * Return the rules of create request.
     *
     * @return array
     */
    public function createRules()
    {
        return [
            'count' => 'required|numeric',
            'cost' => 'required|numeric',
            'bill_id' => 'numeric|exists:bills,id',
        ];
    }

    /**
     * Return the rules of update request.
     *
     * @return array
     */
    public function updateRules()
    {
        $ignoredId = $this->route('product') ? $this->route('product')->id : null;

        return [
            'count' => 'sometimes|numeric',
            'cost' => 'sometimes|numeric',
            'bill_id' => 'sometimes|numeric|exists:bills,id',
        ];

    }

    public function messages()
    {
        return [
            'count.required' => 'قيمة حقل العدد المباع غير صحيحة!',
            'count.numeric' => 'قيمة حقل العدد المباع غير صحيحة!',
            'cost.required' => 'قيمة حقل سعر البيع غير صحيحة!',
            'cost.numeric' => 'قيمة حقل سعر البيع غير صحيحة!',
            'bill_id.exists' => 'قيمة حقل الفاتورة غير صحيحة!',
            'bill_id.numeric' => 'قيمة حقل الفاتورة غير صحيحة!',
        ];
    }


}
