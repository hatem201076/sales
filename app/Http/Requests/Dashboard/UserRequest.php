<?php

namespace App\Http\Requests\Dashboard;

use App\Http\Requests\Dashboard\Request as BaseRequest;

class UserRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->getAproperRules();
    }

    /**
     * Return the rules of create request.
     *
     * @return array
     */
    public function createRules()
    {
        return [
            'name' => 'required|string|max:190',
            'email' => 'required|string|email|max:255|unique:users,email',
            'password' => 'required|string|min:6|confirmed',
            'avatar' => 'mimes:jpg,jpeg,bmp,png,gif',

        ];
    }

    /**
     * Return the rules of update request.
     *
     * @return array
     */
    public function updateRules()
    {
        $ignoredId = $this->route('user') ? $this->route('user')->id : null;

        return [
            'name' => 'sometimes|string|max:190',
            'email' => 'sometimes|string|email|max:255|unique:users,email,' . $ignoredId,
            'password' => 'nullable|string|min:6|confirmed',
            'avatar' => 'mimes:jpg,jpeg,bmp,png,gif',


        ];
    }

    public function attributes()
    {
        return array_dot(trans('managers.attributes'));
    }

    /**
     *
     *
     */
    public function data()
    {
        if ($this->password) {
            return array_merge(parent::all(), [
                'password' => bcrypt($this->password),
            ]);
        }

        return $this->except('password', 'password_confirmation');
    }
}
