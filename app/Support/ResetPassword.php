<?php

namespace App\Support;

use App\Models\User;
use App\Models\ApiPasswordResetCode;
use App\Models\ApiPasswordResetToken;

class ResetPassword
{
    /**
     * Create a verification code and store it into database.
     *
     * @param $email
     * @return string
     */
    public function createVerificationCode($email)
    {
        // Check email verification
        $user = $this->checkEmailActivation($email);
        if (! $user) {
            return false;
        }

        // Find the verification code for this email
        $verification = $this->findCodeByEmail($email);

        // Check if there was a code for this email,
        // and regenerate it
        if (! empty($verification) && $verification instanceof ApiPasswordResetCode) {
            return $this->regenerateCode($user, $verification);
        }

        // Create a new code for this email
        return $this->createCode($user, $email);
    }

    /**
     * Check the code and create token for this user.
     *
     * @param  string $email
     * @param  string $code
     * @return bool|null
     */
    public function checkCode($email, $code)
    {
        // find this code&email
        $verification = $this->findCode($email, $code);

        // Check if exists and not expired
        if (! $verification || $verification->isExpired()) {
            return null;
        }

        // get the user
        $user = User::findByEmail($email)->first();

        $token = $this->getToken();

        // Create token into database
        $user->apiPasswordResetToken()->create([
            'token' => $token,
        ]);

        // Remove the code
        $this->removeCode($email, $code);

        return $token;
    }

    /**
     * Check this token, remove it and return the user.
     *
     * @param string $token
     * @return bool|User
     */
    public function checkToken($token)
    {
        $reset = ApiPasswordResetToken::where('token', $token)->first();

        if (! $reset) {
            return false;
        }

        $this->removeToken($token);

        return $reset->user;
    }

    /**
     * Regenerate verification code for this specific email.
     *
     * @param User $user
     * @param ApiPasswordResetCode $verification
     * @return string
     */
    private function regenerateCode(User $user, ApiPasswordResetCode $verification)
    {
        $code = $this->getCode();

        $verification->update([
            'email' => $verification->email,
            'code' => $code,
        ]);

        $this->sendEmailMessage($user, $code);

        return $code;
    }

    /**
     * Create verification code for this specific email.
     *
     * @param User $user
     * @param $email
     * @return string
     */
    private function createCode(User $user, $email)
    {
        $code = $this->getCode();

        ApiPasswordResetCode::create([
            'email' => $email,
            'code' => $code,
        ]);

        $this->sendEmailMessage($user, $code);

        return $code;
    }

    /**
     * Write message to send as email.
     *
     * @param User $user
     * @param $code
     * @return mixed
     */
    public function sendEmailMessage(User $user, $code)
    {
        // message to send
        $message = sprintf("The verification code for reset password of the email %s is %s \n", $user->email, $code);

        send_email($user, $message);
    }

    /**
     * check if this email has been verified by the user.
     *
     * @param $email
     * @return User|bool
     */
    public function checkEmailActivation($email)
    {
        $user = User::findByEmail($email)->first();

        if ($user->isActivate()) {
            return $user;
        }

        return false;
    }

    /**
     * find the code of this email number.
     *
     * @param $email
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public function findCodeByEmail($email)
    {
        return ApiPasswordResetCode::where('email', $email)->first();
    }

    /**
     * @param $email
     * @param $code
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public function findCode($email, $code)
    {
        return ApiPasswordResetCode::where('email', $email)
            ->where('code', $code)
            ->first();
    }

    /**
     * To remove this code from database.
     *
     * @param $email
     * @param $code
     * @return bool
     */
    public function removeCode($email, $code)
    {
        return ApiPasswordResetCode::where('email', $email)
            ->where('code', $code)
            ->delete();
    }

    /**
     * To remove this user token from database.
     *
     * @param $token
     * @return bool
     */
    public function removeToken($token)
    {
        return ApiPasswordResetToken::where('token', $token)
            ->delete();
    }

    /**
     * Build verification code.
     *
     * @return string
     */
    protected function getCode()
    {
        return str_random(6);
    }

    /**
     * Build verification token.
     *
     * @return string
     */
    protected function getToken()
    {
        return bin2hex(random_bytes(60));
    }
}
