<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Expense;
use Illuminate\Auth\Access\HandlesAuthorization;

class ExpensePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the expenses.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Expense $expense
     * @return mixed
     */
    public function view(User $user, Expense $expense)
    {
        return $user->isAdmin() || $user->isPoss();

    }


    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $user->isAdmin() || $user->isPoss();

    }


    /**
     * Determine whether the user can update the expense.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Expense $expense
     * @return mixed
     */
    public function update(User $user, Expense $expense)
    {
        return $user->isAdmin() || $user->isPoss();

    }


    /**
     * Determine whether the user can delete the expense.
     *
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function delete(User $user)
    {
        return $user->isAdmin() || $user->isPoss();

    }

}
