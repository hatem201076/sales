<?php

namespace App\Policies;

use App\Models\Bill;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class BillPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the clieents.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Bill $bill
     * @return mixed
     */
    public function view(User $user, Bill $bill)
    {
        return $user->isAdmin() || $user->isPoss();

    }


    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $user->isAdmin() || $user->isPoss();

    }


    /**
     * Determine whether the user can update the clients.
     *
     * @param  \App\Models\User $user
     * @param  \Client $client
     * @return mixed
     */
    public function update(User $user, Bill $bill)
    {
        return $user->isAdmin() || $user->isPoss();

    }


    /**
     * Determine whether the user can delete the traders.
     *
     * @param  \App\Models\User $user
     * @param  \Client $client
     * @return mixed
     */
    public function delete(User $user)
    {
        return $user->isAdmin() || $user->isPoss();

    }

}
