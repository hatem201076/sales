<?php

namespace App\Policies;

use App\Models\User;
use \App\Models\Note;
use Illuminate\Auth\Access\HandlesAuthorization;

class NotePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can update the note.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Note $note
     * @return mixed
     */
    public function update(User $user, Note $note)
    {
        return $user->isAdmin() || $user->id == $note->user_id;
    }

    /**
     * Determine whether the user can delete the note.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Note $note
     * @return mixed
     */
    public function delete(User $user, Note $note)
    {
        return $user->isAdmin() || $user->id == $note->user_id;
    }

    /**
     * Determine whether the user can detach him self from the note.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Note $note
     * @return mixed
     */
    public function detach(User $user, Note $note)
    {
        return $user->notes()->find($note->id);
    }
}
