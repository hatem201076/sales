<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Client;
use Illuminate\Auth\Access\HandlesAuthorization;

class ClientPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the clieents.
     *
     * @param  \App\Models\User $user
     * @param  \client $client
     * @return mixed
     */
    public function view(User $user, Client $client)
    {
        return $user->isAdmin() || $user->isPoss();

    }


    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $user->isAdmin() || $user->isPoss();

    }


    /**
     * Determine whether the user can update the clients.
     *
     * @param  \App\Models\User $user
     * @param  \Client $client
     * @return mixed
     */
    public function update(User $user, Client $client)
    {
        return $user->isAdmin() || $user->isPoss();

    }


    /**
     * Determine whether the user can delete the traders.
     *
     * @param  \App\Models\User $user
     * @param  \Client $client
     * @return mixed
     */
    public function delete(User $user)
    {
        return $user->isAdmin() || $user->isPoss();

    }

}
