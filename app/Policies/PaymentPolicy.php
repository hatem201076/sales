<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Payment;
use Illuminate\Auth\Access\HandlesAuthorization;

class PaymentPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the clieents.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Payment $payment
     * @return mixed
     */
    public function view(User $user, Payment $payment)
    {
        return $user->isAdmin() || $user->isPoss();

    }


    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $user->isAdmin() || $user->isPoss();

    }


    /**
     * Determine whether the user can update the clients.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Payment $payment
     * @return mixed
     */
    public function update(User $user, Payment $payment)
    {
        return $user->isAdmin() || $user->isPoss();

    }


    /**
     * Determine whether the user can delete the traders.
     *
     * @param  \App\Models\User $user

     * @return mixed
     */
    public function delete(User $user)
    {
        return $user->isAdmin() || $user->isPoss();

    }

}
