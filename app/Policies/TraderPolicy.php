<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Trader;
use Illuminate\Auth\Access\HandlesAuthorization;

class TraderPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the traders.
     *
     * @param  \App\Models\User $user
     * @param  \trader $trader
     * @return mixed
     */
    public function view(User $user, Trader $trader)
    {
        return $user->isAdmin() || $user->isPoss();

    }


    /**
     * Determine whether the user can create the traders.
     *
     * @param  \App\Models\User $user
     * @param  \trader $trader
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->isAdmin() || $user->isPoss();

    }


    /**
     * Determine whether the user can update the traders.
     *
     * @param  \App\Models\User $user
     * @param  \trader $trader
     * @return mixed
     */
    public function update(User $user, Trader $trader)
    {
        return $user->isAdmin() || $user->isPoss();

    }


    /**
     * Determine whether the user can delete the traders.
     *
     * @param  \App\Models\User $user
     * @param  \trader $trader
     * @return mixed
     */
    public function delete(User $user)
    {
        return $user->isAdmin() || $user->isPoss();

    }

}
