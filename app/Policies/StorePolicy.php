<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Store;
use Illuminate\Auth\Access\HandlesAuthorization;

class StorePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the stores.
     *
     * @param  \App\Models\User $user
     * @param  \store $store
     * @return mixed
     */
    public function view(User $user, Store $store)
    {
        return $user->isAdmin()||$user->isPoss();

    }


    /**
     * Determine whether the user can view the stores.
     *
     * @param  \App\Models\User $user
     * @param  \store $store
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->isAdmin()||$user->isPoss();

    }


    /**
     * Determine whether the user can view the stores.
     *
     * @param  \App\Models\User $user
     * @param  \store $store
     * @return mixed
     */
    public function update(User $user, Store $store)
    {
        return $user->isAdmin()||$user->isPoss();

    }


    /**
     * Determine whether the user can view the stores.
     *
     * @param  \App\Models\User $user
     * @param  \store $store
     * @return mixed
     */
    public function delete(User $user)
    {
        return $user->isAdmin()||$user->isPoss();

    }

}
