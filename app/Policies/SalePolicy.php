<?php

namespace App\Policies;

use App\Models\Sale;
use App\Models\User;
use App\Models\Product;
use Illuminate\Auth\Access\HandlesAuthorization;

class SalePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the product.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Sale $sale
     * @return mixed
     */
    public function view(User $user, Sale $sale)
    {
        return true;
    }

    /**
     * Determine whether the user can create products.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the product.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Product  $product
     * @return mixed
     */
    public function update(User $user, Sale $sale)
    {
        return true;

    }

    /**
     * Determine whether the user can delete the product.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Sale $sale
     * @return mixed
     */
    public function delete(User $user, Sale $sale)
    {
        return $user->isAdmin() || $user->isPoss();
    }


}
