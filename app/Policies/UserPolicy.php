<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
        return $user->isAdmin()|| $user->isPoss();
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \User  $model
     * @return mixed
     */
    public function update(User $auth, User $user)
    {
        return $auth->isAdmin() || $auth->is($user) || $auth->isPoss();
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \User  $model
     * @return mixed
     */
    public function delete(User $user)
    {
        return $user->isAdmin() || $user->isPoss();
    }
}
