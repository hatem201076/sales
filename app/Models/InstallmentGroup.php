<?php

namespace App\Models;

use App\Models\Relations\InstallmentGroupsRelations;
use Illuminate\Database\Eloquent\Model;

class InstallmentGroup extends Model
{
    use InstallmentGroupsRelations;
}
