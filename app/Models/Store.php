<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Relations\StoreRelations;

class Store extends Model
{
    use StoreRelations;

    protected $fillable = ['name'];
}
