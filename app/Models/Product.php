<?php

namespace App\Models;

use App\Models\Concerns\HasMedia;
use App\Models\Relations\ProductRelations;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;

class Product extends Model implements HasMediaConversions
{
    use HasMedia,
        ProductRelations;

    /**
     * Assigne data to database.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'count',
        'wholesale',
        'sector',
        'trader_id',
        'user_id',
        'store_id',
        'description',
        'sale',
        'salecount'
    ];

//    public function getNameAttribute($value)
//    {
//        return  $value." || " .$this->store['name'] ;
//    }
}
