<?php

namespace App\Models;

use App\Models\Concerns\HasMedia;
use App\Models\Scopes\UserScopes;
use App\Models\Helpers\UserHelpers;
use App\Models\Mutators\UserMutators;
use App\Models\Relations\UserRelations;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;

class User extends Authenticatable implements HasMediaConversions
{
    use HasMedia,
        UserHelpers,
        UserMutators,
        UserRelations,
        UserScopes,
        Notifiable;


    /*
    **
    * User of type manager.
    */
    const MANAGER = 1;

    const POSS = 2;

    /**
     * User of type customer.
     */
    const USER = 0;

    /**
     * Variable that assign to database.
     *
     * @var array
     * */
    protected $fillable = [
        'name',
        'email',
        'type',
        'mobile',
        'password',
    ];

}
