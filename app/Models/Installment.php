<?php

namespace App\Models;

use App\Models\Relations\InstallmentRelations;
use Illuminate\Database\Eloquent\Model;

class Installment extends Model
{
    use InstallmentRelations;
   // protected $fillable = ['bill_id'];
}
