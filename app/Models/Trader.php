<?php

namespace App\Models;

use App\Models\Concerns\HasMedia;
use App\Models\Relations\TraderRelations;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;

class Trader extends Model implements HasMediaConversions
{
    use HasMedia, TraderRelations;

    protected $fillable = ['name', 'phone'];
}
