<?php

namespace App\Models\Helpers;

use Pusher;
use App\Models\Reviews;
use App\Models\ServiceToken;
use Illuminate\Http\Request;
use App\Http\Requests\Dashboard\StudentRequest;
use App\Http\Requests\Dashboard\TeacherRequest;

trait UserHelpers
{
    /**
     * set user as admin.
     *
     * @return $this
     */
    public function setAsManager()
    {
        $this->type = 1;

        return $this;
    }

    /**
     * check if the user is admin.
     *
     * @return int
     */
    public function isAdmin()
    {
        return $this->type == 1 ? 1 : 0;
    }

    /**
     * @return int
     */
    public function isPoss()
    {
        return $this->type == 2 ? 2 : 0;
    }

    /**
     * check if the user is not admin.
     *
     * @return bool
     */
    public function isNotAdmin()
    {
        return ! $this->isAdmin();
    }
}
