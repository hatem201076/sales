<?php

namespace App\Models\Relations;


use App\Models\Installment;
use App\Models\Sale;


trait BillRelations
{

    /**
     * return installments of this bill.
     *
     * @return mixed
     */
    public function installments()
    {
         return $this->hasMany(Installment::class, 'bill_id');

    }

    /**
     *  return salesof this bill.

     */
    public function sales()
    {
         return $this->hasMany(Sale::class,'bill_id');

    }
}