<?php

namespace App\Models\Relations;


use App\Models\Collected;
use App\Models\User;
use App\Models\Store;
use App\Models\Trader;
use App\Models\Client;
use App\Models\Product;

trait InstallmentRelations
{

    /**
     * return the user of this product.
     *
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }


    /**
     * @return mixed
     */
    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    /**
     * return the products of this store.
     *
     * @return mixed
     */
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function collecteds()
    {
        return $this->hasMany(Collected::class);
    }


}