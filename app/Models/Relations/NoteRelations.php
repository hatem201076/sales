<?php
/**
 * Created by PhpStorm.
 * User: monem
 * Date: 30/01/18
 * Time: 01:31 م
 */

namespace App\Models\Relations;


use App\Models\Department;
use App\Models\User;

Trait NoteRelations
{
    /**
     * return the user of this note.
     *
     * @return mixed
     */
    public function user()
    {
        return $this->belongTo(User::class);
    }

    /**
     * return department of this note.
     *
     * @return mixed
     */
    public function department()
    {
        return $this->belongTo(Department::class);
    }

}