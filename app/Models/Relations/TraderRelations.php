<?php

namespace App\Models\Relations;


use App\Models\Payment;
use App\Models\Product;

trait TraderRelations
{
    /**
     * return products of this trader.
     *
     * @return mixed
     */
    public function products()
    {
        return $this->hasMany(Product::class);
    }
    /**
     * return products of this trader.
     *
     * @return mixed
     */
    public function payments()
    {
        return $this->hasMany(Payment::class);
    }
}