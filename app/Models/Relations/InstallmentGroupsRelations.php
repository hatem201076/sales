<?php

namespace App\Models\Relations;


use App\Models\Bill;
use App\Models\Installment;
use App\Models\User;
use App\Models\Client;

trait InstallmentGroupsRelations
{
    /**
     * @return mixed
     */
    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function bill()
    {
        return $this->belongsTo(Bill::class, 'bill_id');
    }

    public function group()
    {
        return $this->belongsTo(Installment::class, 'group_id');
    }


    public function installments()
    {
        return $this->hasMany(Installment::class,'group_id');
    }


}