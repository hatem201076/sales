<?php

namespace App\Models\Relations;

use App\Models\Installment;

trait CollectedRelations
{

    /**
     * return the user of this product.
     *
     * @return mixed
     */
    public function installment()
    {
        return $this->belongsTo(Installment::class);
    }





}