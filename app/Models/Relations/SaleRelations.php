<?php

namespace App\Models\Relations;


use App\Models\Product;
use App\Models\User;
use App\Models\Store;
use App\Models\Trader;

trait SaleRelations
{

    /**
     * return the user of this product.
     *
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    /**
     * return the products of this store.
     *
     * @return mixed
     */
    public function product()
    {
            return $this->belongsTo(Product::class,'product_id');
    }


}