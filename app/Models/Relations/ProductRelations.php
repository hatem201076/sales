<?php

namespace App\Models\Relations;


use App\Models\Installment;
use App\Models\Product;
use App\Models\Sale;
use App\Models\User;
use App\Models\Store;
use App\Models\Trader;

trait ProductRelations
{

    /**
     * return the user of this product.
     *
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function trader()
    {
        return $this->belongsTo(Trader::class);
    }

    /**
     * return store of this product.
     *
     * @return mixed
     */
    public function store()
    {
        return $this->belongsTo(Store::class);
    }

//
//    public function sales()
//    {
//        return $this->belongsToMany(Sale::class,'product_sale','product_id','sale_id')->withTimestamps();
//    }
//
//    public function insatallments()
//    {
//        return $this->belongsToMany(Installment::class,'installment_product', 'product_id', 'installment_id')->withTimestamps();
//    }

}