<?php

namespace App\Models\Relations;


use App\Models\Installment;


trait ClientRelations
{

    /**
     * return installments of this client.
     *
     * @return mixed
     */
    public function installments()
    {
         return $this->hasMany(Installment::class);

    }
}