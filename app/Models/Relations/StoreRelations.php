<?php

namespace App\Models\Relations;


use App\Models\Product;

trait StoreRelations
{

    /**
     * return the products of this store.
     *
     * @return mixed
     */
    public function products()
    {
        return $this->hasMany(Product::class);
    }


}