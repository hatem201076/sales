<?php

namespace App\Models\Relations;

use App\Models\City;
use App\Models\Note;
use App\Models\Product;
use App\Models\Review;
use App\Models\Message;
use App\Models\Feedback;
use App\Models\Sale;
use App\Models\ServiceToken;
use App\Models\Specialization;
use App\Models\ApiPasswordResetToken;

trait UserRelations
{
    /**
     * return all department that user role.
     *
     * @return mixed
     */
    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function sales()
    {
        return $this->hasMany(Sale::class);
    }
}
