<?php

namespace App\Models\Relations;

use App\Models\Trader;

trait PaymentRelations
{
    public function trader()
    {
        return $this->belongsTo(Trader::class, 'trader_id');
    }


}