<?php

namespace App\Models\Scopes;

use App\Models\User;
use Illuminate\Http\Request;

trait UserScopes
{
    /**
     * Scope a query to  include Manager.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfTypeManager($query)
    {
        return $query->where('type', User::MANAGER);
    }

    /**
     * Scope a query to  include Customer.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfTypeUser($query)
    {
        return $query->where('type', User::USER);
    }
}
