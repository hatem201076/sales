<?php

namespace App\Models;

use App\Models\Relations\SaleRelations;
use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    use SaleRelations ;
    protected $fillable = [
        'product_id',
        'store_id',
        'sale_count',
        'sale',
        'bill_id'
    ];
}
