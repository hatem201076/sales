<?php

namespace App\Models;

use App\Models\Relations\ClientRelations;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use ClientRelations;
    protected $fillable = ['name', 'phone'];
}
