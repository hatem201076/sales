<?php

namespace App\Models\Mutators;

trait UserMutators
{
    /**
     * Set the type for the user.
     *
     * @param int $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Set the user password after encrypting it.
     *
     * @param string $password
     * @return $this
     */
    public function setPassword($password)
    {
        if ($password) {
            $this->password = bcrypt($password);
        }

        return $this;
    }
}
