<?php

namespace App\Models;

use App\Models\Relations\PaymentRelations;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use PaymentRelations;
    protected $fillable = ['amount', 'date', 'trader_id'];
}
