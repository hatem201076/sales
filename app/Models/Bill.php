<?php

namespace App\Models;

use App\Models\Relations\BillRelations;
use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    use BillRelations;


    const INSTALLMENT = 1;
    const SALE = 2;

    protected $fillable = ['name', 'type'];
}
