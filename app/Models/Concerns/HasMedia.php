<?php

namespace App\Models\Concerns;

use Setting;
use Spatie\MediaLibrary\Media;
use Intervention\Image\Facades\Image;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

trait HasMedia
{
    use HasMediaTrait;

    /**
     * Remove existing media items and add the new base64 fle if preset.
     *
     * @param string $file
     * @param string $collection
     * @return void
     */
    public function addOrUpdateMediaFromBase64($file, $collection = 'default')
    {
        $base64 = request($file);

        // Check if it's base64
        // clear all the media in this collection
        // add the base64 image given
        if ($base64 && base64_decode(base64_encode($base64)) === $base64) {
            $this->clearMediaCollection($collection);
            $this->addMediaFromBase64($base64)->toMediaCollection($collection);
        }
    }

    /**
     * Remove existing media items and add the new one if preset.
     *
     * @param string $name
     * @param string $collection
     * @return void
     */
    public function addOrUpdateMediaFromRequest($name, $collection = 'default')
    {
        $request = request();

        if ($request->hasFile($name)) {
            $this->clearMediaCollection($collection);
            $this->addMediaFromRequest($name)->toMediaCollection($collection);
        }
    }

    /**
     * Remove existing media items and add the new one if preset.
     *
     * @param string $name
     * @param string $collection
     * @return void
     */
    public function addOrUpdateMediaFromUrl($url, $collection = 'default')
    {
        $this->clearMediaCollection($collection);
        $this->addMediaFromUrl($url)->toMediaCollection($collection);
    }

    /**
     * add a new images into the collection.
     *
     * @param string $name
     * @param string $collection
     * @return void
     */
    public function addMultibyteOrBase64MediaFromRequest($name, $collection = 'default')
    {
        $request = request();

        // Handle base64 that coming from request,
        // Like 'image_base64', upload and add to $collection.
        foreach (request($name.'_base64', []) as $file) {
            if (base64_decode(base64_encode($file)) === $file) {
                $this->addMediaFromBase64($file)->toMediaCollection($collection);
            }
        }

        // Handle normal files that coming from request.
        if ($request->hasFile($name)) {
            foreach ($request->$name as $file) {
                $this->addMedia($file)->toMediaCollection($collection);
            }
        }
    }

    /**
     * add a new images into the collection.
     *
     * @param string $name
     * @param string $collection
     * @return void
     */
    public function addMultibyteMediaFromRequest($name, $collection = 'default')
    {
        $request = request();

        // Handle normal files that coming from request.
        if ($request->hasFile($name)) {
            foreach ($request->$name as $file) {
                $this->addMedia($file)->toMediaCollection($collection);
            }
        }
    }

    /**
     * Get the url of the image for the given conversionName
     * for first media for the given collectionName.
     * If  cannot find a media return a default placeholder.
     *
     * @param string $collectionName
     * @param string $conversionName
     * @return string
     */
    public function getFirstOrDefaultMediaUrl(string $collectionName = 'default', $conversionName = '')
    {
        $url = $this->getFirstMediaUrl($collectionName, $conversionName);

        return empty($url) ? 'http://via.placeholder.com/150x150' : $url;
    }

    /**
     * Get the url of the image for the given conversionName
     * for first media for the given collectionName.
     * If  cannot find a media return a default placeholder.
     *
     * @param string $collectionName
     * @return array
     */
    public function getMediaUrls(string $collectionName = 'default')
    {
        $media = $this->getMedia($collectionName);

        $urls = [];
        foreach ($media as $key => $item) {
            $urls[$key]['name'] = $item->file_name;
            $urls[$key]['url'] = url($item->getUrl());
            $urls[$key]['delete_link'] = route('media.delete', $item);
        }

        return $urls;
    }

    /**
     * Register media convertions.
     *
     * @param \Spatie\MediaLibrary\Media|null $media
     * @return void
     */
    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(70)->height(70)->format('png');

        $this->addMediaConversion('small')->width(120)->height(120)->format('png');

        $this->addMediaConversion('medium')->width(160)->height(160)->format('png');

        $this->addMediaConversion('large')->width(320)->height(320)->format('png');
    }
}
