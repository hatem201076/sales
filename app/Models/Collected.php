<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Relations\CollectedRelations;

class Collected extends Model
{
    use CollectedRelations;

    protected $fillable = ['new_collected','date','installment_id'];
}
