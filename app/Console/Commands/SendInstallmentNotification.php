<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Installment;
use Illuminate\Console\Command;
use App\Notifications\InstallmentTimeNotification;
use Illuminate\Support\Facades\Notification;

class SendInstallmentNotification extends Command
{
    public $month;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notifications:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Installment Notification';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $installments = Installment::where('type', '=', 'one')->where('end_date', '>=', Carbon::today())->where(function($query) {
            $query->where('day', '=',Carbon::today()->day);
            $query->orWhere('day', '=',Carbon::today()->day+1);
            $query->orWhere('day', '=',Carbon::today()->day-1);
            $query->orWhere('day', '=',Carbon::today()->day+2);
            $query->orWhere('day', '=',Carbon::today()->day-2);
            $query->orWhere('day', '=',Carbon::today()->day-3);
            $query->orWhere('day', '=',Carbon::today()->day+3);

        })->get();

        $users = User::all();
        foreach ($installments as $installment) {
            Notification::send($users, new InstallmentTimeNotification($installment));
        }
    }


}
