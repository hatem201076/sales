<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        //'App\Model' => 'App\Policies\ModelPolicy',
       'App\Models\User' => 'App\Policies\UserPolicy',
       'App\Models\Store' => 'App\Policies\StorePolicy',
       'App\Models\Trader' => 'App\Policies\TraderPolicy',
       'App\Models\Client' => 'App\Policies\ClientPolicy',
       'App\Models\Product' => 'App\Policies\ProductPolicy',
       'App\Models\Sale' => 'App\Policies\SalePolicy',
       'App\Models\Bill' => 'App\Policies\BillPolicy',
       'App\Models\Payment' => 'App\Policies\PaymentPolicy',
       'App\Models\Expense' => 'App\Policies\ExpensePolicy',
       'App\Models\Installment' => 'App\Policies\InstallmentPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
