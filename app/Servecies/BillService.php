<?php

namespace App\Services;

use App\Models\Installment;

class BillService
{
    /**
     * Get the transactions amount for every main category.
     *
     * @param string $type
     * @param $from
     * @param $to
     */
    public function getInDate($date)
    {
        $installment = Installment::where('type', BankTransaction::DEPOSIT)
            ->sum('amount');

//        $withdrawal = BankTransaction::where('type', BankTransaction::WITHDRAWAL)
//            ->where('date', '<=', $date)
//            ->sum('amount');
//
//        $income = Transaction::where('type', Transaction::INCOME)
//            ->where('date', '<=', $date)
//            ->where('payment_type', Transaction::CHEQUE)
//            ->sum('paid_amount');
//
//        $outcome = Transaction::where('type', Transaction::OUTCOME)
//            ->where('date', '<=', $date)
//            ->where('payment_type', Transaction::CHEQUE)
//            ->sum('paid_amount');
//
//        $bankBalance = ($deposite - $withdrawal) + ($income - $outcome);

        //die(var_dump($bankBalance));

        return compact('deposite', 'withdrawal', 'income', 'outcome', 'bankBalance');
    }
}
