<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Sale::class, function (Faker $faker) {
    static $userId;
    static $productId;
    return [
         'sale_count' => $faker->numberBetween(100, 200),
        'sale' => $faker->numberBetween(1000, 9000),

        'user_id' =>$userId ?? ($userId = factory('App\Models\User')->create()->id),
        'product_id' =>$productId ?? ($productId = factory('App\Models\Product')->create()->id),
       ];
});
