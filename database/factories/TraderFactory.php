<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Trader::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'phone' => $faker->creditCardNumber,
    ];
});
