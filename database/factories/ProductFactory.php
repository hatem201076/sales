<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Product::class, function (Faker $faker) {
    static $userId;
    static $storeId;
    static $traderId;
    return [

        'name' => $faker->word,
        'count' => $faker->numberBetween(1000, 9000),
        'wholesale' => $faker->numberBetween(1000, 9000),
        'sector' => $faker->numberBetween(1000, 9000),
        'trader_id' => $traderId ?? ($traderId = factory('App\Models\Trader')->create()->id),
        'user_id' =>$userId ?? ($userId = factory('App\Models\User')->create()->id),
        'store_id' =>$storeId ?? ($stored = factory('App\Models\Store')->create()->id ),
        'description' => $faker->paragraph,
    ];

});
