<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Client::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'phone' => $faker->creditCardNumber,
    ];
});
