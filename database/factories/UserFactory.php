<?php

use Faker\Generator as Faker;

$factory->define(App\Models\User::class, function (Faker $faker) {

    return [

        'name' => $faker->word,
        'email' => $faker->email,
        'password' => bcrypt('123456'),
        'type' => \App\Models\User::USER
    ];

});
