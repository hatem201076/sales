<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('count');
            $table->decimal('wholesale', 8, 2);
            $table->decimal('sector', 8, 2);
            $table->integer('all_sector');
            $table->integer('all_wholesale');
            $table->string('description')->nullable();

            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('trader_id')->nullable();
            $table->unsignedInteger('store_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
            $table->foreign('trader_id')->references('id')->on('traders')->onDelete('set null');
            $table->foreign('store_id')->references('id')->on('stores')->onDelete('set null');
            $table->timestamps();
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
