<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCollectedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collecteds', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('collected');
            $table->date('date');
            $table->unsignedInteger('installment_id')->nullable();

            $table->foreign('installment_id')->references('id')->on('installments')->onDelete('set null');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collecteds');
    }
}
