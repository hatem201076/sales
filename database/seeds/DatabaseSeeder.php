<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $user = User::firstOrCreate([
            'name' => 'Administrator',
            'email' => 'admin@gmail.com',
            'mobile' => 01025071255,
        ], [
            'password' => bcrypt('123456'),
        ]);

        $user->type = User::POSS;
        $user->save();
        factory(\App\Models\User::class, 2)->create();
        factory(\App\Models\Store::class, 2)->create();
        factory(\App\Models\Trader::class, 2)->create();
        factory(\App\Models\Product::class, 2)->create();
        factory(\App\Models\Sale::class, 0)->create();
        \App\Models\Bill::create(
            [
                'name' => 'installmen bill',
                'type' => 1,
            ]
        );
        \App\Models\Bill::create(
            [
                'name' => 'sale bill',
                'type' => 2,
            ]
        );
        factory(\App\Models\Client::class, 2)->create();
    }
}
